﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Teachesser.Model;
using Teachesser.Util;
using System;

namespace TeachesserTest
{
    [TestClass]
    public class TestDeplacements
    {
        Plateau p = new Plateau();
        List<Deplacement> deplacements = new List<Deplacement>();
        Deplacement temp = new Deplacement();
        
        
        //[TestMethod]
        //public void TestDeplacementsPossiblesPionBlanc()
        //{
        //    ConsoleManager.Show();
            
        //    p.plateauDepart();
        //    // ON teste sans prise
        //    deplacements.AddRange(p.coupsPossibles(Case.A2));
        //    // Nombre de coups possibles
        //    Assert.AreEqual(2,deplacements.Count());
        //    temp.caseSource = Case.A2;
        //    temp.caseDestination = Case.A3;
        //    temp.pieceDeplacee = Piece.PION_BLANC;
        //    temp.typeDeplacement = TypeDeplacement.DEPLACEMENT_NORMAL;
        //    temp.pieceCapturee = Piece.CASE_VIDE;
        //    // Contient déplacement
        //    Assert.AreEqual(true, deplacements.Contains(temp));

        //    // On rajoute une pièce ennemi
        //    p.ajouterPiece(Case.B3, Piece.PION_NOIR);
        //    deplacements.Clear();
        //    deplacements.AddRange(p.coupsPossibles(Case.A2));
        //    temp.caseSource = Case.A2;
        //    temp.caseDestination = Case.B3;
        //    temp.pieceDeplacee = Piece.PION_BLANC;
        //    temp.typeDeplacement = TypeDeplacement.DEPLACEMENT_CAPTURE_ORDINAIRE;
        //    temp.pieceCapturee = Piece.PION_NOIR;
        //    Assert.AreEqual(true, deplacements.Contains(temp));

        //    // Si on est sur la dernière case
        //    p = new Plateau();
        //    p.ajouterPiece(Case.A8, Piece.PION_BLANC);
        //    Assert.AreEqual(0, p.coupsPossibles(Case.A8).Count());       
        //}

        //[TestMethod]
        //public void TestDeplacementsPossiblesPionNoir()
        //{
            
        //    p.plateauDepart();
        //    deplacements.AddRange(p.coupsPossibles(Case.A7));
        //    Assert.AreEqual(2, deplacements.Count());
        //    temp.caseSource = Case.A7;
        //    temp.caseDestination = Case.A5;
        //    temp.pieceDeplacee = Piece.PION_NOIR;
        //    temp.typeDeplacement = TypeDeplacement.DEPLACEMENT_NORMAL;
        //    temp.pieceCapturee = Piece.CASE_VIDE;
            
        //    // Contient déplacement
        //    Assert.AreEqual(true, deplacements.Contains(temp));

        //    // On rajoute une pièce ennemi
        //    p.ajouterPiece(Case.B6, Piece.PION_BLANC);
        //    deplacements.Clear();
        //    deplacements.AddRange(p.coupsPossibles(Case.A7));
        //    temp.caseSource = Case.A7;
        //    temp.caseDestination = Case.B6;
        //    temp.pieceDeplacee = Piece.PION_NOIR;
        //    temp.typeDeplacement = TypeDeplacement.DEPLACEMENT_CAPTURE_ORDINAIRE;
        //    temp.pieceCapturee = Piece.PION_BLANC;
        //    Assert.AreEqual(true, deplacements.Contains(temp));

        //    // Si on est sur la dernière case
        //    p = new Plateau();
        //    p.ajouterPiece(Case.A1, Piece.PION_BLANC);
        //    Assert.AreEqual(0, p.coupsPossibles(Case.A8).Count());

        //}

        //[TestMethod]
        //public void TestJouerDeplacementOrdinaire()
        //{
        //    ConsoleManager.Show();
        //    p.plateauDepart();
        //    int hash = 0;
        //    temp = new Deplacement(Case.A2, Case.A4, Piece.PION_BLANC);
        //    hash = p.HashKey();
        //    p.jouerMouvement(temp);
            
        //    Assert.AreEqual(p.trouverPiece(Case.A2), Piece.CASE_VIDE);
        //    Assert.AreEqual(p.trouverPiece(Case.A4), Piece.PION_BLANC);

        //    Assert.AreNotEqual<int>(p.HashKey(), hash);
            
        //}

        //[TestMethod]
        //public void TestJouerDeplacementCapture()
        //{
        //    ConsoleManager.Show();
        //    p.plateauDepart();
        //    int hash = 0;
        //    p.ajouterPiece(Case.A3,Piece.PION_NOIR);
        //    deplacements  =p.coupsPossibles(Case.B2);
        //    temp = deplacements.Find(x => x.typeDeplacement == TypeDeplacement.DEPLACEMENT_CAPTURE_ORDINAIRE);
        //    Console.WriteLine(temp);
            

        //    hash = p.HashKey();
        //    p.jouerMouvement(temp);

        //    Assert.AreEqual(p.trouverPiece(Case.A3), Piece.PION_BLANC);
        //    Assert.AreEqual(p.trouverPiece(Case.B2), Piece.CASE_VIDE);

        //    Assert.AreNotEqual<int>(p.HashKey(), hash);

        //}
    }
}
