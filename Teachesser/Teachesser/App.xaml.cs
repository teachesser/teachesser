using System.Windows;
using System.Windows.Media;

namespace Teachesser
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region CONSTANTES DE L'APPLICATION
        //Couleurs
        public static Color BLEU_COLOR  = (Color)ColorConverter.ConvertFromString("#13428E");
        public static Color ROUGE_COLOR = (Color)ColorConverter.ConvertFromString("#BF3452");
        public static Color VERT_COLOR  = (Color)ColorConverter.ConvertFromString("#25886A");
        public static Color BLANC_COLOR = (Color)ColorConverter.ConvertFromString("#FFFFC9");

        public static Brush BLEU_BRUSH  = new SolidColorBrush(BLEU_COLOR);
        public static Brush ROUGE_BRUSH = new SolidColorBrush(ROUGE_COLOR);
        //public static Brush ROUGE_BRUSH = System.Windows.Media.Brushes.Red;
        public static Brush VERT_BRUSH  = new SolidColorBrush(VERT_COLOR);
        public static Brush BLANC_BRUSH = new SolidColorBrush(BLANC_COLOR);

        //Taille de la fenetre 

        public static double HAUTEUR = System.Windows.SystemParameters.PrimaryScreenHeight;
        public static double LARGEUR = System.Windows.SystemParameters.PrimaryScreenWidth;

        public static double MI_HAUTEUR = HAUTEUR / 2;
        public static double MI_LARGEUR = LARGEUR / 2;


        //Plateau
        public static int HAUTEURCASE    = 40;
        #endregion
    }
}