﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Surface.Presentation.Controls;
using System.Collections;
using Teachesser.View;
using System.Windows.Controls;
using Teachesser.View.InterfaceJeu;

namespace Teachesser.Model
{
    class CaseDefinition
    {
        //une case
        public Case c;
        //un TagVisualizer
        public TagVisualizer visu;
        public Image image;
        
      
        //liste d'objets casedefiniton, avec le numéro de la case, et son tagVisualiser associé
        public static List<CaseDefinition> listeCase = new List<CaseDefinition>();


        //constructeur qui va ajouter dans la liste un couple case/TagVisualizer
        private CaseDefinition(Case c, TagVisualizer visu, Image image)
        {
            this.c = c;
            this.visu = visu;
            this.image = image;
            listeCase.Add(this);
  
        }

        //on va ici aller chercher dans le .xaml de l'interfacePlateau tous les noms des tagVisualizer
        //afin de les associer aux cases de la l'énumération
        public CaseDefinition(InterfacePlateau ip)
        {
            
            for (Case c = Case.A8; c <= Case.H1; c++)
            {
                //on crée une string dynamique, comme le format est respecté pour chaque case
                String labelName = string.Format("TagCase{0}", c);
                String imageName = string.Format("ImageCase{0}", c);
                //et on va chercher le bon tag correspondant au bon nom dans le .xaml
                TagVisualizer visu = (TagVisualizer)ip.FindName(labelName);
                Image image = (Image)ip.FindName(imageName);
                
                //on crée ensuite une intance de CaseDefinition
                new CaseDefinition(c, visu,image);
            }

           
            

            //ici, pour chaque Instance de CaseDefinition, on associe au tagVisualizer sa définition :
            foreach (CaseDefinition cd in listeCase)
            {

                //chaque case va pouvoir accueillir n'importe quel type de pion
                for (int c = 0; c < 12; c++)
                {
                    TagVisualizationDefinition tvd = new TagVisualizationDefinition();
                    //on lui donne sa valeur
                    tvd.Value = c;
                    //le omportement à l'ajout/retrait du byte tag
                    tvd.TagRemovedBehavior = TagRemovedBehavior.Disappear;
                    //le fait qu'il disparraisse instantanément
                    tvd.LostTagTimeout = 0;
                    //ajout de l'image
                    tvd.Source = new Uri("Resources/"+c+".xaml", UriKind.Relative );
                    //tvd.Source = new Uri("Resources/TagVisualization1.xaml", UriKind.Relative);
                    //ajout ici
                    cd.visu.Definitions.Add(tvd);
                }

            }
        }

        /*private CaseDefinition(InterfacePlateau ip)
        {

            for (Case c = Case.A8; c <= Case.H1; c++)
            {
                //on crée une string dynamique, comme le format est respecté pour chaque case
                String labelName = string.Format("TagCase{0}", c);
                //et on va chercher le bon tag correspondant au bon nom dans le .xaml
                TagVisualizer visu = (TagVisualizer)ip.FindName(labelName);

                //on crée ensuite une intance de CaseDefinition
                new CaseDefinition(c, visu);
            }

            Grid g = (Grid)ip.FindName("nomGrid");
            g.Height = 840;
            g.Width = g.Height;


            //ici, pour chaque Instance de CaseDefinition, on associe au tagVisualizer sa définition :
            foreach (CaseDefinition cd in listeCase)
            {

                //chaque case va pouvoir accueillir n'importe quel type de pion
                if (cd.c >= Case.A8 && cd.c <= Case.H7 || cd.c >= Case.A2 && cd.c <= Case.H1)
                {
                    TagVisualizationDefinition tvd = new TagVisualizationDefinition();
                    //on lui donne sa valeur
                    tvd.Value = 0;
                    //le omportement à l'ajout/retrait du byte tag
                    tvd.TagRemovedBehavior = TagRemovedBehavior.Disappear;
                    //le fait qu'il disparraisse instantanément
                    tvd.LostTagTimeout = 0;
                    //ajout de l'image
                    tvd.Source = new Uri("Resources/0.xaml", UriKind.Relative);
                    //tvd.Source = new Uri("Resources/TagVisualization1.xaml", UriKind.Relative);
                    //ajout ici
                    cd.visu.Definitions.Add(tvd);
                }

            }
        }*/

        /// <summary>
        /// Retourne un tagVisualizer
        /// </summary>
        /// <param name="c">Case</param>
        /// <returns></returns>
        #region getVisu
        public static TagVisualizer getVisu(Case c)
        {
            TagVisualizer visu = null;
            foreach (CaseDefinition cd in listeCase)
            {
                if (cd.c == c)
                    visu = cd.visu;
                
            }
            return visu;

        }
        #endregion

        /// <summary>
        /// Retourne une case 
        /// </summary>
        /// <param name="visu">TagVisualizer</param>
        /// <returns></returns>
        #region getCase
        public static Case getCase(TagVisualizer visu)
        {
            Case c = (Case)(-1);
            foreach (CaseDefinition cd in listeCase)
            {
                if (cd.visu == visu)
                    c = cd.c;
            }
            return c;

        }
        #endregion
    }
}
