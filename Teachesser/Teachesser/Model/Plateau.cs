﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Teachesser.Model.IntelligenceArtificielle;

using System.Collections;
using System.Diagnostics;


namespace Teachesser.Model
{
    public class Plateau
    {
        // Constantes utiles
        public static readonly int NB_CASES = 64;
        public static readonly int NB_PIECES = 12;
        public static readonly int NB_BITBOARDS = 14;
        private int[] valeurMateriel;
        private int[] nbPion;

        // Joueur courant (0 : Blancs, 1 : Noirs)
        public int JoueurCourant;
        // Dernier deplacement (sert aux prises en passant)
        public Deplacement dernierDeplacement;
        
	    // Tableau de nombres aléatoires utiles pour les tables de transpositions
        // et l'algo de Zolish
        private static int[,] _hashKeyComponents;
        // Celui-ci sert pour la détéction de collision
        private static int[,] _hashLockComponents;

        //Evaluation de la valeurs des pièces
        public static int[] valeurPieces;

        // Tableau contenant les 64 bits associes a chaque case du plateau
        // ex : la premiere case (superieure gauche) : 1000 0000 ... 0000
        // la seconde case : 0100 0000 ... 0000, derniere case : 0000 ... 0000 0001
        public static long[] BitsCases;
        public long[] bitboards;

        /// <summary>
        /// Liste des pièces noires capturées par les blancs (mais, c'est de l'esclavage ! )
        /// </summary>
        public static List<Piece> listeCaptureBlancs = new List<Piece>();

        /// <summary>
        /// Liste des pièces blanches capturées par les noirs (mais, c'est des pirates !)
        /// </summary>
        public static List<Piece> listeCaptureNoir = new List<Piece>();

        // Tableau servant aux roques
        // La premiere case sert aux blancs, la seconde aux noirs
        public bool[] peutPetitRoque = new bool[2];
        public bool[] peutGrandRoque = new bool[2];
        // tableau qui nous dit si un coté a roqué (0 blanc 1 noir)
        public bool[] aRoque = new bool[2];
        public bool echecMat;
        public bool pat;
        public bool tempsFini;

        
        // Constructeur statique qui sert à initialiser les membres statiques une seule fois
        static Plateau()
        {
            // Initialisation du tableau bitsCases
            BitsCases = new long[NB_CASES];
            for (int i = 0; i < NB_CASES; i++)
            {
                BitsCases[i] = (1L << i);
            }
            
            // On remplit la table de transpo avec des nombres aléatoires
            HashKeyComponents = new int[NB_PIECES, NB_CASES];
            HashLockComponents = new int[NB_PIECES, NB_CASES];
            Random rnd = new Random();
            for (int i = 0; i < NB_PIECES; i++)
            {
                for (int j = 0; j < NB_CASES; j++)
                {
                    HashKeyComponents[i, j] = rnd.Next();
                    HashLockComponents[i, j] = rnd.Next();
                }
            }

            // Initialisation de la valeur des pièces
            valeurPieces = new int[NB_PIECES];
            valeurPieces[(int)Piece.PION_BLANC] = 100;
            valeurPieces[(int)Piece.PION_NOIR]  = 100;
            valeurPieces[(int)Piece.CAVALIER_BLANC] = 300;
            valeurPieces[(int)Piece.CAVALIER_NOIR] = 300;
            valeurPieces[(int)Piece.FOU_BLANC] = 350;
            valeurPieces[(int)Piece.FOU_NOIR] = 350;
            valeurPieces[(int)Piece.TOUR_BLANCHE] = 500;
            valeurPieces[(int)Piece.TOUR_NOIRE] = 500;
            valeurPieces[(int)Piece.DAME_BLANCHE] = 900;
            valeurPieces[(int)Piece.DAME_NOIRE] = 900;
            valeurPieces[(int)Piece.ROI_BLANC] = 2000;
            valeurPieces[(int)Piece.ROI_NOIR] = 2000;
        }

        public Plateau()
        {
            bitboards = new long[NB_BITBOARDS];
            valeurMateriel = new int[2];
            nbPion = new int[2];
            peutPetitRoque[0] = true;
            peutPetitRoque[1] = true;
            peutGrandRoque[0] = true;
            peutGrandRoque[1] = true;
            aRoque[0] = false;
            aRoque[1] = false;
            echecMat = false;
            pat = false;
            tempsFini = false;
        }

        /// <summary>
        /// Place les pièces sur le tableau
        /// </summary>
        public int plateauDepart()
        {
            viderPlateau();

            ajouterPiece(Case.A8, Piece.TOUR_NOIRE);
            ajouterPiece(Case.B8, Piece.CAVALIER_NOIR);
            ajouterPiece(Case.C8, Piece.FOU_NOIR);
            ajouterPiece(Case.D8, Piece.DAME_NOIRE);
            ajouterPiece(Case.E8, Piece.ROI_NOIR);
            ajouterPiece(Case.F8, Piece.FOU_NOIR);
            ajouterPiece(Case.G8, Piece.CAVALIER_NOIR);
            ajouterPiece(Case.H8, Piece.TOUR_NOIRE);

            for (int i = (int) Case.A7; i <= (int) Case.H7; i++)
            {
                ajouterPiece((Case)i, Piece.PION_NOIR);
            }

            for (int i = (int) Case.A2; i <= (int) Case.H2; i++)
            {
                ajouterPiece((Case)i, Piece.PION_BLANC);
            }

            ajouterPiece(Case.A1, Piece.TOUR_BLANCHE);
            ajouterPiece(Case.B1, Piece.CAVALIER_BLANC);
            ajouterPiece(Case.C1, Piece.FOU_BLANC);
            ajouterPiece(Case.D1, Piece.DAME_BLANCHE);
            ajouterPiece(Case.E1, Piece.ROI_BLANC);
            ajouterPiece(Case.F1, Piece.FOU_BLANC);
            ajouterPiece(Case.G1, Piece.CAVALIER_BLANC);
            ajouterPiece(Case.H1, Piece.TOUR_BLANCHE);

            // Definition du joueur courant

            //SetCurrentPlayer(jcPlayer.SIDE_WHITE);
            JoueurCourant = Joueur.COTE_BLANC;
            return 32;
        }

        /// <summary>
        /// Evalue l'avantage (ou non) materiel du point de vue d'un coté
        /// </summary>
        /// <param name="cote">Le cote du joueur</param>
        /// <returns>la différence entre le joeur courant et l'autre</returns>
        public int evalMateriel(int cote)
        {
            // Si les deux joeurs ont la meme valeur alors la diff =0
            if (valeurMateriel[Joueur.COTE_BLANC]==valeurMateriel[Joueur.COTE_NOIR])
            {
                return 0;
            }
            int autreCote = (cote + 1) % 2;
            int valeurMatTotale = valeurMateriel[cote] + valeurMateriel[autreCote];
            // Qui est en train de gagner ?
            if (valeurMateriel[Joueur.COTE_NOIR] > valeurMateriel[Joueur.COTE_BLANC])
            {
                //Les noirs gagnent 
                int diffMat = valeurMateriel[Joueur.COTE_NOIR] - valeurMateriel[Joueur.COTE_BLANC];
                //Fonction d'évaluation classique de la valeur d'un plateau
                int val = Math.Min(2400,diffMat) + ( diffMat *(12000-valeurMatTotale)* nbPion[Joueur.COTE_NOIR])
                    /(6400 * ( nbPion[Joueur.COTE_NOIR] +1));
                if (cote == Joueur.COTE_NOIR)
                    return val;
                else
                    return -val;
            }
            else
            {
                int diffMat = valeurMateriel[Joueur.COTE_BLANC] - valeurMateriel[Joueur.COTE_NOIR];
                int val = Math.Min(2400, diffMat) + (diffMat * (12000 - valeurMatTotale) * nbPion[Joueur.COTE_BLANC])
                    / (6400 * (nbPion[Joueur.COTE_BLANC] + 1));
                if (cote == Joueur.COTE_BLANC)
                    return val;
                else
                    return -val;

            }
        }

        /// <summary>
        /// Trouve la pièce qui est sur une case
        /// </summary>
        /// <param name="indexCase"></param>
        /// <returns></returns>
        public Piece trouverPiece(Case indexCase)
        {
            /*if (indexCase == Case.NULL_CASE)
                return Piece.CASE_VIDE;*/
            // Recherche de la piece
            
            for (int i = (int) Piece.PION_BLANC; i <= (int) Piece.ROI_NOIR; i++)
            {
                
                if ((bitboards[i] & BitsCases[(int) indexCase]) != 0)
                    return (Piece) i;
            }

            // Aucune piece trouvee, lancement d'une exception
            return Piece.CASE_VIDE;
        }

        /// <summary>
        /// Dit si une case est libre ou s'il y a une piece (noire ou blanche dessus)
        /// </summary>
        /// <param name="indexCase">La case que l'on veut tester</param>
        /// <returns>vrai si libre faux sinon</returns>
        public bool caseEstLibre(Case indexCase)
        {
            if (indexCase < Case.A8 || indexCase > Case.H1)
                return false;
            if (((bitboards[(int)Piece.PIECES_BLANCHES] & BitsCases[(int)indexCase]) == 0) &&
                ((bitboards[(int)Piece.PIECES_NOIRES] & BitsCases[(int)indexCase]) == 0))
                return true;
            return false;
        }

        public List<Deplacement> coupsPossibles(Case indexCase)
        {
            List<Deplacement> cases = new List<Deplacement>();
            Case i = indexCase;
            Piece piece = trouverPiece(indexCase);
            switch(piece) {
                case Piece.TOUR_BLANCHE :
                case Piece.TOUR_NOIRE :
                    cases = Deplacement.CoupsPossiblesTour(indexCase, this);
                    break;
                case Piece.CAVALIER_BLANC :
                case Piece.CAVALIER_NOIR :
                    cases = Deplacement.CoupsPossiblesCavalier(this, indexCase );
                    break;
                case Piece.FOU_BLANC :
                case Piece.FOU_NOIR :
                    cases = Deplacement.CoupsPossiblesFou(this,indexCase);
                    break;
                case Piece.DAME_BLANCHE :
                case Piece.DAME_NOIRE :
                    cases = Deplacement.CoupsPossiblesTour(indexCase, this);
                    cases.AddRange(Deplacement.CoupsPossiblesFou(this,indexCase));
                    break;
                case Piece.ROI_BLANC :
                case Piece.ROI_NOIR :
                    
                    cases = Deplacement.CoupsPossiblesRoi(this,indexCase);
                    
                    break;
                case Piece.PION_BLANC :
                    cases = Deplacement.CoupsPossiblesPionBlanc(this,indexCase);
                    break;
                case Piece.PION_NOIR :
                    cases = Deplacement.CoupsPossiblesPionNoir(this,indexCase);
                    break;
            }
            return cases;
        }

        public long ensembleCapturesPossibles(Piece couleur)
        {
            long cases = 0;
            
            for (Case c = Case.A8; c <= Case.H1; c++)
            {
                if ((bitboards[(int)couleur] & BitsCases[(int)c]) != 0)
                {
                    Piece piece = trouverPiece(c);

                    switch (piece)
                    {
                        case Piece.TOUR_BLANCHE:
                        case Piece.TOUR_NOIRE:
                            cases |= Deplacement.CasesPossiblesTour(this,c);
                            break;
                        case Piece.CAVALIER_BLANC:
                        case Piece.CAVALIER_NOIR:
                            cases |= Deplacement.CasesPossiblesCavalier(this,c);
                            break;
                        case Piece.FOU_BLANC:
                        case Piece.FOU_NOIR:
                            cases |= Deplacement.CasesPossiblesFou(this,c);
                            break;
                        case Piece.DAME_BLANCHE:
                        case Piece.DAME_NOIRE:
                            cases |= Deplacement.CasesPossiblesTour(this,c);
                            cases |= Deplacement.CasesPossiblesFou(this,c);
                            break;
                        case Piece.ROI_BLANC:
                        case Piece.ROI_NOIR:
                            cases |= Deplacement.CasesPossiblesRoi(this,c);
                            break;
                        case Piece.PION_BLANC:
                            cases |= Deplacement.CapturesPossiblesPionBlanc(this,c);
                            break;
                        case Piece.PION_NOIR:
                            cases |= Deplacement.CapturesPossiblesPionNoir(this,c);
                            break;
                    }
                }
            }

            return cases;
        }

        public List<Deplacement> tousLesCoupsPossibleCouleur(bool avecPrise)
        {
            List<Deplacement> deplacements = new List<Deplacement>();
            Piece p;
            if (JoueurCourant == Joueur.COTE_BLANC)
            {
                for (Case c = Case.A8; c <= Case.H1; c++)
                {
                    p = trouverPiece(c);
                    // Si la pièce est blanche
                    if ((int)p % 2 == 0 && p!=Piece.CASE_VIDE)
                    {
                        deplacements.AddRange(coupsPossibles(c));
                    }
                }
            }
            
            else if (JoueurCourant == Joueur.COTE_NOIR)
            {
                
                for (Case c = Case.A8; c <= Case.H1; c++)
                {
                    
                    p = trouverPiece(c);
                    // Si la pièce est noir
                    if ((int)p % 2 == 1 && p != Piece.CASE_VIDE)
                    {
                        deplacements.AddRange(coupsPossibles(c));
                    }
                }
            }
            else
            {
                return null;
            }
            if (avecPrise)
            {
                List<Deplacement> deplacementsAvecPrise = new List<Deplacement>();
                foreach (Deplacement dep in deplacements)
                {
                    if (dep.typeDeplacement == TypeDeplacement.DEPLACEMENT_CAPTURE_ORDINAIRE)
                    {
                        deplacementsAvecPrise.Add(dep);
                    }
                }
                return deplacementsAvecPrise;
            }
            
            return deplacements;
        }

        /// <summary>
        /// Permet de savoir si un roi est en echec
        /// </summary>
        /// <param name="roi">Le roi de la couleur a tester</param>
        /// <returns>Renvoie vrai si le roi est en echec, faux sinon</returns>
        public bool estEnEchec(Piece roi)
        {
            return (ensembleCapturesPossibles(Piece.PIECES_NOIRES - ((int)roi % 2)) & bitboards[(int)roi]) != 0;
        }

        public void jouerMouvement(Deplacement dep)
        {
           switch (dep.typeDeplacement)
            {
                case TypeDeplacement.DEPLACEMENT_NORMAL:
                    retirerPiece(dep.caseSource, dep.pieceDeplacee);
                    ajouterPiece(dep.caseDestination, dep.pieceDeplacee);
                    break;
                case TypeDeplacement.DEPLACEMENT_CAPTURE_ORDINAIRE:
                    retirerPiece(dep.caseSource, dep.pieceDeplacee);
                    retirerPiece(dep.caseDestination, dep.pieceCapturee);
                    ajouterPiece(dep.caseDestination, dep.pieceDeplacee);
                    break;
                case TypeDeplacement.DEPLACEMENT_PETIT_ROQUE:
                    retirerPiece(dep.caseSource, dep.pieceDeplacee);
                    ajouterPiece(dep.caseDestination, dep.pieceDeplacee);
                    retirerPiece(dep.caseSource + 3, Piece.TOUR_BLANCHE + (int)dep.pieceDeplacee % 2);
                    ajouterPiece(dep.caseSource + 1, Piece.TOUR_BLANCHE + (int)dep.pieceDeplacee % 2);
                    // PE je met ca la en attendant faut ptete le mettre autre part
                    aRoque[(int)dep.pieceDeplacee % 2] = true;
                    break;
                case TypeDeplacement.DEPLACEMENT_GRAND_ROQUE:
                    retirerPiece(dep.caseSource, dep.pieceDeplacee);
                    ajouterPiece(dep.caseDestination, dep.pieceDeplacee);
                    retirerPiece(dep.caseSource - 4, Piece.TOUR_BLANCHE + (int)dep.pieceDeplacee % 2);
                    ajouterPiece(dep.caseSource - 1, Piece.TOUR_BLANCHE + (int)dep.pieceDeplacee % 2);
                    aRoque[(int)dep.pieceDeplacee % 2] = true;
                    break;
                default:
                    break;
            }

            // Verifications liees aux roques
            if ((dep.pieceDeplacee == Piece.ROI_BLANC || dep.pieceDeplacee == Piece.ROI_NOIR) && peutPetitRoque[0 + (int)dep.pieceDeplacee % 2])
            {
                peutPetitRoque[0 + (int)dep.pieceDeplacee % 2] = false;
                peutGrandRoque[0 + (int)dep.pieceDeplacee % 2] = false;
            }
            if (dep.pieceDeplacee == Piece.TOUR_BLANCHE || dep.pieceDeplacee == Piece.TOUR_NOIRE)
            {
                if ((dep.caseSource == Case.A1 || dep.caseSource == Case.A8) && peutGrandRoque[0 + (int)dep.pieceDeplacee % 2])
                {
                    peutGrandRoque[0 + (int)dep.pieceDeplacee % 2] = false;
                }
                else if ((dep.caseSource == Case.H1 || dep.caseSource == Case.H8) && peutPetitRoque[0 + (int)dep.pieceDeplacee % 2])
                {
                    peutPetitRoque[0 + (int)dep.pieceDeplacee % 2] = false;
                }
            }
            
            // Enregistrement dernier deplacement
            dernierDeplacement = dep;

            // Changement de joueur
            JoueurCourant = (JoueurCourant + 1) % 2;
            
            // Verification echec et mat ou pat
            bool peutJouer = false;
            
            List<Deplacement> coupsPossibles = tousLesCoupsPossibleCouleur(false);
            
            foreach (Deplacement d in coupsPossibles)
            {
                if (d.typeDeplacement != TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                {
                    peutJouer = true;
                    break;
                }
            }
            
            // Si aucun coup n'est possible pour le nouveau joueur : situation de pat ou de mat
            if (!peutJouer)
            {
                // Si le roi du nouveau joueur est en echec, il y a echec et mat
                if (estEnEchec(Piece.ROI_BLANC + JoueurCourant))
                {
                    echecMat = true;
                }
                // Sinon il y a pat
                else
                {
                    pat = true;
                }
            }
        }

        public void promouvoirPion(Case indexCase, Piece piecePromue, Piece piecePromotion)
        {
            retirerPiece(indexCase, piecePromue);
            ajouterPiece(indexCase, piecePromotion);
        }

        public void ajouterPiece(Case indexCase, Piece piece)
        {
            // Ajout de la piece au bitboard correspondant
            bitboards[(int)piece] |= BitsCases[(int)indexCase];

            // Ajout de la piece au bitboard contenant toutes les pieces d'une meme couleur
            // (Si l'index de la piece est pair, alors c'est une piece blanche, sinon noire)
            bitboards[(int)Piece.PIECES_BLANCHES + ((int)piece % 2)] |= BitsCases[(int)indexCase];

            // On met a jour la balance materielle
            valeurMateriel[(int)piece % 2] += valeurPieces[(int)piece];
            if (piece == Piece.PION_BLANC)
                nbPion[Joueur.COTE_BLANC]++;
            else if (piece == Piece.PION_NOIR)
                nbPion[Joueur.COTE_NOIR]++;
        }

        public void retirerPiece(Case indexCase, Piece piece)
        {
            // Suppression de la piece du bitboard correspondant
            bitboards[(int)piece] ^= BitsCases[(int)indexCase];
            // Suppression de la piece du bitboard contenant toutes les pieces d'une meme couleur
            bitboards[(int)Piece.PIECES_BLANCHES + ((int)piece % 2)] ^= BitsCases[(int)indexCase];
            
            // On met a jour la balance materielle
            valeurMateriel[(int)piece % 2] -= valeurPieces[(int)piece];
            if (piece == Piece.PION_BLANC)
                nbPion[Joueur.COTE_BLANC]--;
            else if (piece == Piece.PION_NOIR)
                nbPion[Joueur.COTE_NOIR]--;
        }

        // Methodes privees
        private void viderPlateau()
        {
            // Initialisation de chaque bitboard a 0
            for (int i = 0; i < NB_BITBOARDS; i++)
            {
                bitboards[i] = 0;
            }
        }

        public bool Clone(Plateau target)
        {
            /*EnPassantPawn = target.EnPassantPawn;
            for (int i = 0; i < 4; i++)
            {
                CastlingStatus[i] = target.CastlingStatus[i];
            }*/
            for (int i = 0; i < NB_BITBOARDS; i++)
            {
                bitboards[i] = target.bitboards[i];
            }
            valeurMateriel[0] = target.valeurMateriel[0];
            valeurMateriel[1] = target.valeurMateriel[1];
            nbPion [0] = target.nbPion[0];
            nbPion [1] = target.nbPion[1];
            peutGrandRoque[0] = target.peutGrandRoque[0];
            peutGrandRoque[1] = target.peutGrandRoque[1];
            peutPetitRoque[0] = target.peutPetitRoque[0];
            peutPetitRoque[1] = target.peutPetitRoque[1];
            aRoque[0] = target.aRoque[0];
            aRoque[1] = target.aRoque[1];

            /*
            ExtraKings[0] = target.ExtraKings[0];
            ExtraKings[1] = target.ExtraKings[1];
            HasCastled[0] = target.HasCastled[0];
            HasCastled[1] = target.HasCastled[1];*/
            JoueurCourant = target.JoueurCourant;
            
            return true;
        }

        public int ChangerCote()
        {
            if (JoueurCourant == Joueur.COTE_BLANC)
                JoueurCourant = Joueur.COTE_NOIR;
            else
                JoueurCourant = Joueur.COTE_BLANC;

            return JoueurCourant;
        }

        public List<Case> casesOccupees()
        {
            List<Case> casesOccupees = new List<Case>(); 
            for (int i = 0; i < NB_CASES; i++)
            {
                if (trouverPiece((Case)i) != Piece.CASE_VIDE)
                    casesOccupees.Add((Case) i);
            }
            return casesOccupees;
        }

        public bool AfficherPlateau()
        {
            String piecesBlanches = Convert.ToString(bitboards[(int)Piece.PIECES_BLANCHES], 2);
            String piecesNoires = Convert.ToString(bitboards[(int)Piece.PIECES_NOIRES], 2);
            /*
            char[] piecesBlanchesChar = piecesBlanches.ToCharArray();
            char[] piecesNoiresChar = piecesNoires.ToCharArray();
            */Console.WriteLine("PIECES BLANCHES");
            Console.WriteLine(piecesBlanches);/*
            for (int i =0; i < 64 ; i++)
            {
                if (i % 8 == 0 && i != 0)
                    Console.Write("|\n");
                if (i >= piecesBlanches.Length)
                    Console.Write("| 0 ");
                else
                    Console.Write("| " + piecesBlanchesChar[i] + " ");
                
            }*/
            Console.WriteLine("\nPIECES NOIRES");
            Console.WriteLine(piecesNoires);/*
            for (int i = 0; i < 64; i++)
            {
                if (i % 8 == 0 && i != 0)
                    Console.Write("|\n");
                if (i >= piecesNoiresChar.Length)
                    Console.Write("| 0 ");
                else
                    Console.Write("| " + piecesNoiresChar[i] + " ");
            }

            Console.WriteLine("\n"+(bitboards[(int)Piece.PIECES_BLANCHES] & bitboards[(int)Piece.PIECES_NOIRES]));*/
            for (int line = 0; line < 8; line++)
			{
			    Console.WriteLine("\n-----------------------------------------");
                Console.WriteLine("|    |    |    |    |    |    |    |    |");
                for (int col = 0; col < 8; col++)
                {
                    long bits = BitsCases[line * 8 + col];
                    Piece piece = Piece.PION_BLANC;
                    while (((int)piece < NB_PIECES ) && (( bits & bitboards[(int)piece]) == 0))
                        piece++;

                    if (piece == Piece.PIECES_BLANCHES)
                    {
                        Console.Write("| -- ");
                    }
                    else
                        Console.Write("| " + piece.getString() + " ");
                    
                }
                Console.WriteLine("|");
                Console.WriteLine("|    |    |    |    |    |    |    |    |");

            }
            Console.WriteLine("-----------------------------------------");
            if (JoueurCourant == Joueur.COTE_BLANC)
                Console.WriteLine("JOUEUR BLANC");
            if (JoueurCourant == Joueur.COTE_NOIR)
                Console.WriteLine("JOUEUR NOIR");

			
            
            return true;
        }
        public void afficherPlateauCouleurPiece()
        {
            for (int line = 0; line < 8; line++)
            {
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine("|    |    |    |    |    |    |    |    |");
                for (int col = 0; col < 8; col++)
                {
                    long bits = BitsCases[line * 8 + col];
                    Piece piece = Piece.PION_BLANC;
                    while (((int)piece < NB_PIECES) && ((bits & bitboards[(int)piece]) == 0))
                        piece++;

                    if (piece == Piece.PIECES_BLANCHES)
                    {
                        Console.Write("| -- ");
                    }
                    else if ((bitboards[(int)piece] & bitboards[(int)Piece.PIECES_BLANCHES]) != 0)
                        Console.Write("|PBS ");
                    else if ((bitboards[(int)piece] & bitboards[(int)Piece.PIECES_NOIRES]) != 0)
                    {
                        Console.Write("|PNS ");
                    }

                }
                Console.WriteLine("|");
                Console.WriteLine("|    |    |    |    |    |    |    |    |");

            }
            Console.WriteLine("-----------------------------------------");
            if (JoueurCourant == Joueur.COTE_BLANC)
                Console.WriteLine("JOUEUR BLANC");
            if (JoueurCourant == Joueur.COTE_NOIR)
                Console.WriteLine("JOUEUR NOIR");

            for (int i = 0; i < BitsCases.Length; i++)
                Console.WriteLine(Convert.ToString(BitsCases[i], 2));
            Console.WriteLine(BitsCases.Length+"\n\n\n");
        }

        public int HashKey()
        {
            int hash = 0;
            for (int currPiece = 0; currPiece < NB_PIECES; currPiece++)
            {
                long bitboardTmp = bitboards[currPiece];
                for (int currCase=0; currCase < NB_CASES; currCase++)
                {
                    if ((bitboardTmp & BitsCases[currCase]) != 0)
                        hash ^= HashKeyComponents[currPiece,currCase];
                }
            }

            return hash;
        }

        public int HashLock()
        {
            int hash = 0;
            for (int currPiece = 0; currPiece < NB_PIECES; currPiece++)
            {
                long bitboardTmp = bitboards[currPiece];
                for (int currCase = 0; currCase < NB_CASES; currCase++)
                {
                    if ((bitboardTmp & BitsCases[currCase]) != 0)
                        hash ^= HashLockComponents[currPiece, currCase];
                }
            }

            return hash;
        }

        // GETTERS ET SETTERS
        #region getters and setters
        public static int[,] HashKeyComponents
        {
            get { return _hashKeyComponents; }
            set { _hashKeyComponents = value; }
        }

        public static int[,] HashLockComponents
        {
            get { return _hashLockComponents; }
            set { _hashLockComponents = value; }
        }
        #endregion

        // Main de test

        public int plateauTest()
        {
            viderPlateau();
            ajouterPiece(Case.A5, Piece.PION_BLANC);
            ajouterPiece(Case.C5, Piece.PION_BLANC);
            ajouterPiece(Case.B3, Piece.PION_NOIR);
            JoueurCourant = 0;

            return 3;
        }

        public int plateauPresentation()
        {
            // Place les pieces sur le plateau
            viderPlateau();

            ajouterPiece(Case.G3, Piece.CAVALIER_NOIR);
            ajouterPiece(Case.A8, Piece.TOUR_BLANCHE);
            ajouterPiece(Case.G1, Piece.CAVALIER_BLANC);
            ajouterPiece(Case.H8, Piece.FOU_NOIR);

            return 4;
        }
        
        static void Main()
        {
            Plateau plateau = new Plateau();
           
            Console.WriteLine(Convert.ToString(plateau.bitboards[(int)Piece.PIECES_BLANCHES], 2));
            
            Console.WriteLine(Convert.ToString(plateau.bitboards[(int)Piece.PIECES_BLANCHES], 2));
            
            //Console.SetWindowSize(100, 58);

            Console.WriteLine(plateau.evalMateriel(plateau.JoueurCourant));

            List<Deplacement> cases = new List<Deplacement>();
           
            plateau.plateauDepart();
            
            AlphaBetaChercheur chercheur = new AlphaBetaChercheur();
            long tempsTotal = 0;
            int i = 0;
            while (!plateau.echecMat && !plateau.pat)
            {
                Stopwatch watch = Stopwatch.StartNew();
                i++;
                
                Console.WriteLine(Convert.ToString(plateau.bitboards[(int)Piece.PIECES_BLANCHES], 2));
                Console.WriteLine("nb Tour " + i );
                Deplacement dep = chercheur.choisirMeilleurDeplacement(plateau);
                if (dep.typeDeplacement == TypeDeplacement.DEPLACEMENT_ABANDON)
                {
                    Console.WriteLine("ABANDON");
                    break;
                }
                //Console.WriteLine(dep);
                plateau.jouerMouvement(dep);
                Console.WriteLine(dep);
                plateau.AfficherPlateau();
                Console.WriteLine(chercheur.NumRegularNodes + " cutoff " + chercheur.NumRegularCutoffs + " Tranquille " + chercheur.NumQuiescenceNodes);
                Console.WriteLine(chercheur.NumQuiescenceTTHits + " TTranquille " + chercheur.NumRegularTTHits + " TT " + chercheur.NumQuiescenceNodes);
                //Console.ReadLine();
                watch.Stop();
                long elapsedMs = watch.ElapsedMilliseconds;
                tempsTotal += elapsedMs;
                Console.WriteLine("Temps : "+elapsedMs);
                //Console.ReadLine();
            }
            plateau.AfficherPlateau();
            Console.WriteLine(chercheur.NumRegularNodes + " cutoff " + chercheur.NumRegularCutoffs + " Tranquille " + chercheur.NumQuiescenceNodes);
            Console.WriteLine("Temps Total (en secondes) " + tempsTotal / 1000 + "Temps Moyen " + tempsTotal / (i * 1000));
            Console.ReadLine();
        }   
    }
}