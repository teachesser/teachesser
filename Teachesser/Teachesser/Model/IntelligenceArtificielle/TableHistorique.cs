﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teachesser.Model.IntelligenceArtificielle
{
    class ComparateurDeplacement : IComparer<Deplacement>
    {
        public int Compare(Deplacement ob1, Deplacement ob2)
        {
            TableHistorique tableHist = TableHistorique.GetInstance();
            Deplacement mov1 = (Deplacement)ob1;
            Deplacement mov2 = (Deplacement)ob2;
            if (tableHist.historiqueCourant[(int)mov1.caseSource][ (int)mov1.caseDestination] >
            tableHist.historiqueCourant[(int)mov2.caseSource][ (int)mov2.caseDestination])
                return -1;
            else
                return 1;
        }
    }
    
    class TableHistorique
    {
        int [][][]historique ;
        public int [][]historiqueCourant;
        static TableHistorique singleton = new TableHistorique();
        public static TableHistorique GetInstance()
        {
            return singleton;
        }
        public bool TrierDeplacement(ref List<Deplacement> theList, int movingPlayer)
        {
            historiqueCourant = historique[movingPlayer];
            ComparateurDeplacement comp = new ComparateurDeplacement();
            theList.Sort(comp);
            return true;
        }
        public bool Forget()
        {
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 64; j++)
                    for (int k = 0; k < 64; k++)
                        historique[i][j][k] = 0;
            return true;
        }
        // History table compilation
        public bool AddCount(int whichPlayer, Deplacement mov)
        {
            historique[whichPlayer][(int)mov.caseSource][(int)mov.caseDestination]++;
            return true;
        }
        // Constructeur privé
        TableHistorique()
        {
            historique = new int[2][][];
            for (int i = 0; i < 2; i++)
			{
                historique[i] = new int[64][];
			    for (int j = 0; j < 64; j++)
			    {
			        historique[i][j] = new int [64];
			    }
			}
            
        }
        

    }
}
