﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace Teachesser.Model.IntelligenceArtificielle
{
    public class AlphaBetaChercheur
    {
        TableTransposition tableTrans;

        TableHistorique tableHistorique;

        EvaluateurPlateau evaluateur;
        int pointDeVueQui;

        // MAXNODE correspond au tour de l'ia et MINNODE au joueur
        static readonly bool MAXNODE = true;
        static readonly bool MINNODE = false;


        // Frontières de l'alphabeta
        static readonly int ALPHABETA_MAXVAL = 30000;
        static readonly int ALPHABETA_MINVAL = -30000;
        static readonly int ALPHABETA_ILLEGAL = -31000;

        static readonly int SEUIL_EVAL = 200;

        static readonly int ALPHABETA_ABANDON = -29995;
        Random rnd;

        // Statistics ptete pas utile mais bon tjrs interessant
        public int NumRegularNodes;
        public int NumQuiescenceNodes;
        public int NumRegularTTHits;
        public int NumQuiescenceTTHits;
        public int NumRegularCutoffs;
        public int NumQuiescenceCutoffs;

        public static int profondeurRecherche=3;

        // Compteur de mouvement qui sert à la table de transposition
        int MoveCounter;

        //pour indiquer l'état de progression de l'IA dans le calcul sur l'interface
        int compteurProgression;
        

        public Deplacement choisirMeilleurDeplacement(Plateau plateau, BackgroundWorker bw)
        {
            compteurProgression = 0;
            pointDeVueQui = plateau.JoueurCourant;
            MoveCounter++;
            if (rnd.Next(4) % 2 == 0)
            {
                tableHistorique.Forget();
            }
            NumRegularNodes = 0; NumQuiescenceNodes = 0;
            NumRegularTTHits = 0; NumQuiescenceTTHits = 0;
            Deplacement leDeplacement = new Deplacement();
            List<Deplacement>  deplacements = new List<Deplacement>();
            deplacements = plateau.tousLesCoupsPossibleCouleur(false);
            tableHistorique.TrierDeplacement(ref deplacements, (int)plateau.JoueurCourant);

            int meilleurPourLeMoment = ALPHABETA_MINVAL;
            Plateau newBoard = new Plateau();
            int currentAlpha = ALPHABETA_MINVAL;
            int nbDep = deplacements.Count();
            foreach (Deplacement dep in deplacements)
            {
                bw.ReportProgress(Convert.ToInt32(((double)compteurProgression / deplacements.Count) * 100));
                compteurProgression++;
                //Console.WriteLine(dep);
                newBoard.Clone(plateau);
                //Console.WriteLine("plateau " + newBoard.evalMateriel(newBoard.JoueurCourant)+ newBoard);
                newBoard.jouerMouvement(dep);

                int scoreDeplacement = AlphaBeta(MINNODE, newBoard, 1, currentAlpha, ALPHABETA_MAXVAL);
                //Console.WriteLine(scoreDeplacement);

                if (scoreDeplacement == ALPHABETA_ILLEGAL)
                    continue;
                if (dep.typeDeplacement == TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                    continue;
                currentAlpha = Math.Max(currentAlpha, scoreDeplacement);
                if (scoreDeplacement > meilleurPourLeMoment)
                {
                    leDeplacement.Copy(dep);
                    meilleurPourLeMoment = scoreDeplacement;
                    leDeplacement.evaluationDeplacement = scoreDeplacement;
                }
                Console.Write(deplacements.IndexOf(dep) + "/" + nbDep +"\r");
                
            }
            if (meilleurPourLeMoment <= ALPHABETA_ABANDON)
            {
                leDeplacement.typeDeplacement = TypeDeplacement.DEPLACEMENT_ABANDON;
            }
            Console.WriteLine(NumRegularTTHits);
            //Console.WriteLine("nbMove "+MoveCounter);

            return leDeplacement;
        }

        public Deplacement choisirMeilleurDeplacement(Plateau plateau)
        {
            
            pointDeVueQui = plateau.JoueurCourant;
            MoveCounter++;
            if (rnd.Next(4) % 2 == 0)
            {
                tableHistorique.Forget();
            }
            NumRegularNodes = 0; NumQuiescenceNodes = 0;
            NumRegularTTHits = 0; NumQuiescenceTTHits = 0;
            Deplacement leDeplacement = new Deplacement();
            List<Deplacement>  deplacements = new List<Deplacement>();
            deplacements = plateau.tousLesCoupsPossibleCouleur(false);
            tableHistorique.TrierDeplacement(ref deplacements, (int)plateau.JoueurCourant);

            int meilleurPourLeMoment = ALPHABETA_MINVAL;
            Plateau newBoard = new Plateau();
            int currentAlpha = ALPHABETA_MINVAL;
            int nbDep = deplacements.Count();
            foreach (Deplacement dep in deplacements)
            {
                
                
                //Console.WriteLine(dep);
                newBoard.Clone(plateau);
                //Console.WriteLine("plateau " + newBoard.evalMateriel(newBoard.JoueurCourant)+ newBoard);
                newBoard.jouerMouvement(dep);

                int scoreDeplacement = AlphaBeta(MINNODE, newBoard, profondeurRecherche, currentAlpha, ALPHABETA_MAXVAL);
                //Console.WriteLine(scoreDeplacement);

                if (scoreDeplacement == ALPHABETA_ILLEGAL)
                    continue;
                if (dep.typeDeplacement == TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                    continue;
                currentAlpha = Math.Max(currentAlpha, scoreDeplacement);
                if (scoreDeplacement > meilleurPourLeMoment)
                {
                    leDeplacement.Copy(dep);
                    meilleurPourLeMoment = scoreDeplacement;
                    leDeplacement.evaluationDeplacement = scoreDeplacement;
                }
                Console.Write(deplacements.IndexOf(dep) + "/" + nbDep + "\r");

            }
            if (meilleurPourLeMoment <= ALPHABETA_ABANDON)
            {
                leDeplacement.typeDeplacement = TypeDeplacement.DEPLACEMENT_ABANDON;
            }
            Console.WriteLine(NumRegularTTHits);
            //Console.WriteLine("nbMove "+MoveCounter);

            return leDeplacement;
        }
        public AlphaBetaChercheur()
        {
            tableTrans = new TableTransposition();
            tableHistorique = TableHistorique.GetInstance();
            evaluateur = new EvaluateurPlateau();
            rnd = new Random();
            MoveCounter = 0;
        }

        public int AlphaBeta(bool nodeType, Plateau plateau, int profondeur, int alpha, int beta)
        {
            Deplacement dep = tableTrans.chercherPlateau(plateau);
            NumRegularNodes++;
            if (dep!=null && (dep.profondeurRecherche >= profondeur))
            {
                if (nodeType == MAXNODE)
                {
                    if ((dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_PRECISE) ||
                         (dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_MINORANTE))
                    {
                        if (dep.evaluationDeplacement >= beta)
                        {
                            NumRegularTTHits++;
                            return dep.evaluationDeplacement;
                        }
                    }
                }
                else
                {
                    if ((dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_PRECISE) ||
                         (dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_MAJORANTE))
                    {
                        if (dep.evaluationDeplacement <= alpha)
                        {
                            NumRegularTTHits++;
                            return dep.evaluationDeplacement;
                        }
                    }
                }
            }
            if (profondeur == 0)
                return AlphaBetaTranquil(nodeType, plateau,5, alpha, beta);

            List<Deplacement> deplacements = plateau.tousLesCoupsPossibleCouleur(false);

            if (deplacements.Count() == 0)
            {
                return ALPHABETA_ILLEGAL;
            }
            tableHistorique.TrierDeplacement(ref deplacements, plateau.JoueurCourant);
            Plateau p = new Plateau();
            int meilleurPourLeMoment;

            if (nodeType == MAXNODE)
            {
                meilleurPourLeMoment = ALPHABETA_MINVAL;
                int alphaCourant = alpha;
                // Pour tout les déplacements possibles
                foreach (Deplacement deplacement in deplacements)
                {
                    if (deplacement.typeDeplacement == TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                        continue;
                    //On crée un nouveau plateau
                    p.Clone(plateau);
                    // On joue le movement sur ce plateau
                    p.jouerMouvement(deplacement);
                    //On évalue le déplacement avec type de node inverse (algo alphaBeta)
                    int scoreDeplacement = AlphaBeta(!nodeType, p, profondeur - 1, alphaCourant, beta);
                    // Si c'est illegal on passe au coup suivant
                    if (scoreDeplacement == ALPHABETA_ILLEGAL)
                        continue;
                    // alpha devient le max entre lui et le score de deplacement
                    alphaCourant = Math.Max(alphaCourant, scoreDeplacement);
                    // Si le score est le meilleur de tout les autres
                    if (scoreDeplacement > meilleurPourLeMoment)
                    {
                        // on actualise le meilleur
                        meilleurPourLeMoment = scoreDeplacement;
                        // S'il est meme meilleur que beta on le stocke dans la table de transpo et on le renvoit
                        if (meilleurPourLeMoment >= beta)
                        {
                            tableTrans.sauvegarderPlateau(plateau, meilleurPourLeMoment, (int)TypeEvaluationDeplacement.EVALUATION_MAJORANTE, profondeur, MoveCounter);
                            tableHistorique.AddCount(plateau.JoueurCourant, deplacement);
                            NumRegularCutoffs++;
                            return meilleurPourLeMoment;
                        }
                    }
                }
                
                // On teste si il y a un échec ce qui arrive s'il n'y pas de coups légaux
                // C'est à dire si meilleurPourLeMoment == Minval
                if (meilleurPourLeMoment <= ALPHABETA_MINVAL)
                {
                    if (p.echecMat)
                    {
                        return meilleurPourLeMoment + profondeur;;
                    }
                    if (p.pat)
                    {
                        return 0;
                    }
                }
            }
            // On est sur le noeud d'un min
            else
            {
                meilleurPourLeMoment = ALPHABETA_MAXVAL;
                int betaCourant = beta;
                foreach (Deplacement depl in deplacements)
                {
                    if (depl.typeDeplacement == TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                        continue;
                    p.Clone(plateau);
                    p.jouerMouvement(depl);
                    int scoreDeplacement = AlphaBeta(!nodeType, p, profondeur - 1, alpha, betaCourant);
                    if (scoreDeplacement == ALPHABETA_ILLEGAL)
                    {
                        continue;
                    }
                    betaCourant = Math.Min(betaCourant, meilleurPourLeMoment);
                    if (scoreDeplacement < meilleurPourLeMoment)
                    {
                        meilleurPourLeMoment = scoreDeplacement;
                        if (meilleurPourLeMoment <= alpha)
                        {
                            tableTrans.sauvegarderPlateau(plateau, meilleurPourLeMoment, (int)TypeEvaluationDeplacement.EVALUATION_MAJORANTE, profondeur, MoveCounter);
                            tableHistorique.AddCount(p.JoueurCourant, depl);
                            NumRegularCutoffs++;
                            return meilleurPourLeMoment;
                        }
                    }
                }
                if (meilleurPourLeMoment >= ALPHABETA_MAXVAL)
                {
                    // On teste si il y a un échec ce qui arrive s'il n'y pas de coups légaux
                    // C'est à dire si meilleurPourLeMoment == Minval
                    if (p.echecMat)
                    {
                        return meilleurPourLeMoment + profondeur;
                    }
                    if (p.pat)
                    {
                        return 0;
                    }
                }

            }
            // On arrive la si on a trouvé une évaluation correcte qui n'est pas un échec
            tableTrans.sauvegarderPlateau(plateau, meilleurPourLeMoment, (int)TypeEvaluationDeplacement.EVALUATION_PRECISE, profondeur, MoveCounter);
            return meilleurPourLeMoment;
        }

        public int AlphaBetaTranquil(bool nodeType, Plateau plateau,int profondeur, int alpha, int beta)
        {
            Deplacement dep = tableTrans.chercherPlateau(plateau);
            NumQuiescenceNodes++;
            if (dep != null)
            {
                if (nodeType == MAXNODE)
                {
                    if ((dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_PRECISE) ||
                         (dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_MINORANTE))
                    {
                        if (dep.evaluationDeplacement >= beta)
                        {
                            NumQuiescenceTTHits++;
                            return dep.evaluationDeplacement;
                        }
                    }
                }
                else
                {
                    if ((dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_PRECISE) ||
                         (dep.typeEvaluationDeplacement == (int)TypeEvaluationDeplacement.EVALUATION_MAJORANTE))
                    {
                        if (dep.evaluationDeplacement <= alpha)
                        {
                            NumQuiescenceTTHits++;
                            return dep.evaluationDeplacement;
                        }
                    }
                }
            }
            int meilleurPourLeMoment = ALPHABETA_MINVAL;
            meilleurPourLeMoment = evaluateur.evaluationRapide(plateau,pointDeVueQui);
            if ((meilleurPourLeMoment > (beta + SEUIL_EVAL)) || (meilleurPourLeMoment < (alpha - SEUIL_EVAL)))
                return meilleurPourLeMoment;
            else
               meilleurPourLeMoment = evaluateur.evaluationComplete(plateau, pointDeVueQui);
            
            if (profondeur == 0)
                return meilleurPourLeMoment;
            List<Deplacement> deplacements = plateau.tousLesCoupsPossibleCouleur(true);
            
            // S'il n'y a aucune capture
            if (deplacements.Count == 0)
            {
                return meilleurPourLeMoment;
            }

            Plateau nouveauPlateau = new Plateau();
            if (nodeType == MAXNODE)
            {
                int alphaCourant = alpha;
                foreach (Deplacement depl in deplacements)
                {
                    if (depl.typeDeplacement == TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                        continue;
                    nouveauPlateau.Clone(plateau);
                    nouveauPlateau.jouerMouvement(depl);

                    int scoreDeplacement = AlphaBetaTranquil(!nodeType, nouveauPlateau,profondeur-1, alphaCourant, beta);
                    if (scoreDeplacement == ALPHABETA_ILLEGAL)
                        continue;
                    alphaCourant = Math.Max(alphaCourant, scoreDeplacement);
                    if (scoreDeplacement > meilleurPourLeMoment)
                    {
                        meilleurPourLeMoment = scoreDeplacement;
                        if (meilleurPourLeMoment>= beta)
                        {
		                    tableTrans.sauvegarderPlateau(plateau, meilleurPourLeMoment,(int) TypeEvaluationDeplacement.EVALUATION_MAJORANTE, 0, MoveCounter);
                            tableHistorique.AddCount(plateau.JoueurCourant,depl);
                            NumQuiescenceCutoffs ++;
                            return meilleurPourLeMoment;
	                    }
                    }
                }
                if (meilleurPourLeMoment <= ALPHABETA_MINVAL)
                {
                    // On teste si il y a un échec ce qui arrive s'il n'y pas de coups légaux
                    // C'est à dire si meilleurPourLeMoment == Minval
                    if (nouveauPlateau.echecMat)
                    {
                        return meilleurPourLeMoment + profondeur;
                    }
                    if (nouveauPlateau.pat)
                    {
                        return 0;
                    }
                }
            }
            else
            {
                int betaCourant = beta;
                foreach (Deplacement depl in deplacements)
                {
                    if (depl.typeDeplacement == TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                        continue;
                    nouveauPlateau.Clone(plateau);
                    nouveauPlateau.jouerMouvement(depl);

                    int scoreDeplacement = AlphaBetaTranquil(!nodeType, nouveauPlateau,profondeur-1, alpha, betaCourant);
                    if (scoreDeplacement == ALPHABETA_ILLEGAL)
                        continue;
                    betaCourant = Math.Min(betaCourant, scoreDeplacement);
                    if (scoreDeplacement < meilleurPourLeMoment)
                    {
                        meilleurPourLeMoment = scoreDeplacement;
                        if (meilleurPourLeMoment <= alpha)
                        {
                            tableTrans.sauvegarderPlateau(plateau, meilleurPourLeMoment, (int)TypeEvaluationDeplacement.EVALUATION_MAJORANTE, 0, MoveCounter);
                            tableHistorique.AddCount(plateau.JoueurCourant, depl);
                            NumQuiescenceCutoffs++;
                            return meilleurPourLeMoment;
                        }
                    }
                }
                if (meilleurPourLeMoment >= ALPHABETA_MAXVAL)
                {
                    // On teste si il y a un échec ce qui arrive s'il n'y pas de coups légaux
                    // C'est à dire si meilleurPourLeMoment == Maxval
                    if (nouveauPlateau.echecMat)
                    {
                        return meilleurPourLeMoment + profondeur;
                    }
                    if (nouveauPlateau.pat)
                    {
                        return 0;
                    }
                }

                
            }

            tableTrans.sauvegarderPlateau(plateau, meilleurPourLeMoment, (int)TypeEvaluationDeplacement.EVALUATION_PRECISE, 0, MoveCounter);
            return meilleurPourLeMoment;

           }
    }
}