﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teachesser.Model.IntelligenceArtificielle
{
    class EvaluateurPlateau
    {
        int Grain =3;
        public int evaluationRapide(Plateau p, int pointDeVueQui)
        {
            return ((p.evalMateriel(pointDeVueQui)));
        }

        // Fonction qui sert surtout au début de la partie et permet de savoir si 
        // le joueur a bien dévelloper ses pièces (i.e. occuper le centre avec pions, cav et fou);
        private int EvalDeveloppement(Plateau p, int pointDeVueQui)
        {
            int score = 0;
            if (pointDeVueQui == Joueur.COTE_BLANC)
            {
                //Si les pions sont encores sur leurs cases de départ c'est pas ouf
                if (p.trouverPiece(Case.D2) == Piece.PION_BLANC)
                    score -= 15;
                if (p.trouverPiece(Case.E2) == Piece.PION_BLANC)
                    score -= 15;
                // Si les cavaliers et les fous sont encore derriere c'est pas super non plus
                for (Case square = Case.A1; square <= Case.H1; square++)
                {
                    if (p.trouverPiece(square) == Piece.CAVALIER_BLANC || p.trouverPiece (square) == Piece.FOU_BLANC)
                        score -= 10;
                }

                long bitboardDame = p.bitboards[(int)Piece.DAME_BLANCHE];
                // Si il y a encore une dame et qu'elle n'est plus sur sa case 
                // Il faut vérifier qu'elle n'as pas quitté sa backline trop tot
                // Du coup pour chaques piece encore sur sa case originale on diminue le score
                if ((bitboardDame != 0) && (p.trouverPiece(Case.D1) != Piece.DAME_BLANCHE))
                {
                    int nbPieceCaseOriginale=0;
                    if ( p.trouverPiece(Case.A1) == Piece.TOUR_BLANCHE)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.B1) == Piece.CAVALIER_BLANC)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.C1) == Piece.FOU_BLANC)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.E1) == Piece.ROI_BLANC)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.F1) == Piece.FOU_BLANC)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.G1) == Piece.CAVALIER_BLANC)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.H1) == Piece.TOUR_BLANCHE)
                        nbPieceCaseOriginale++;
                    score-= (nbPieceCaseOriginale << 3);
                }
                // On incite le roque qui permet de protéger son roi et de libérer les tours
                if (p.bitboards[(int)Piece.DAME_NOIRE] != 0)
                {
                    // Avoir roqué donne un bonus
                    if (p.aRoque[Joueur.COTE_BLANC])
                        score += 10;
                    // petite pénalité si on peut roquer des deux cotés
                    else if (p.peutPetitRoque[Joueur.COTE_BLANC]  &&
                              p.peutGrandRoque[Joueur.COTE_BLANC])
                        score -= 24;
                    // Plus grosse pénalité si on peut le faire que du coté du roi #petitRoque
                    else if (p.peutPetitRoque[Joueur.COTE_BLANC])
                        score -= 40;
                    // Encore plus grosse pénalité si on peut le faire que du coté de la dame #grandRoque
                    else if (p.peutGrandRoque[Joueur.COTE_BLANC])
                        score -= 80;
                    // Encore plus grosse pénalité si on ne peut plus roquer
                    else
                        score -= 120;
                }
                

            }
            else
	        {
                 //Si les pions sont encores sur leurs cases de départ c'est pas ouf
                if (p.trouverPiece(Case.D7) == Piece.PION_NOIR)
                    score -= 15;
                if (p.trouverPiece(Case.E7) == Piece.PION_NOIR)
                    score -= 15;
                // Si les cavaliers et les fous sont encore derriere c'est pas super non plus
                for (Case square = Case.A8; square <= Case.H8; square++)
                {
                    if (p.trouverPiece(square) == Piece.CAVALIER_NOIR || p.trouverPiece (square) == Piece.FOU_NOIR)
                        score -= 10;
                }

                long bitboardDame = p.bitboards[(int)Piece.DAME_NOIRE];
                // Si il y a encore une dame et qu'elle n'est plus sur sa case 
                // Il faut vérifier qu'elle n'as pas quitté sa backline trop tot
                // Du coup pour chaques piece encore sur sa case originale on diminue le score
                if ((bitboardDame != 0) && (p.trouverPiece(Case.D1) != Piece.DAME_NOIRE))
                {
                    int nbPieceCaseOriginale=0;
                    if ( p.trouverPiece(Case.A8) == Piece.TOUR_NOIRE)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.B8) == Piece.CAVALIER_NOIR)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.C8) == Piece.FOU_NOIR)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.E8) == Piece.ROI_NOIR)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.F8) == Piece.FOU_NOIR)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.G8) == Piece.CAVALIER_NOIR)
                        nbPieceCaseOriginale++;
                    if ( p.trouverPiece(Case.H8) == Piece.TOUR_NOIRE)
                        nbPieceCaseOriginale++;
                    score-= (nbPieceCaseOriginale << 3);
                }
                // On incite le roque qui permet de protéger son roi et de libérer les tours
                if (p.bitboards[(int)Piece.DAME_BLANCHE] != 0)
                {
                    // Avoir roqué donne un bonus
                    if (p.aRoque[Joueur.COTE_NOIR])
                        score += 10;
                    // petite pénalité si on peut roquer des deux cotés
                    else if (p.peutPetitRoque[Joueur.COTE_NOIR]  &&
                                p.peutGrandRoque[Joueur.COTE_NOIR])
                        score -= 24;
                    // Plus grosse pénalité si on peut le faire que du coté du roi #petitRoque
                    else if (p.peutPetitRoque[Joueur.COTE_NOIR])
                        score -= 40;
                    // Encore plus grosse pénalité si on peut le faire que du coté de la dame #grandRoque
                    else if (p.peutGrandRoque[Joueur.COTE_NOIR])
                        score -= 80;
                    // Encore plus grosse pénalité si on ne peut plus roquer
                    else
                        score -= 120;
                }
	        }

            return score;
        }

        // private EvalRookBonus
        // Rooks are more effective on the seventh rank, on open files and behind
        // passed pawns
        private int evalTourBonus(Plateau plateau, int pointDeVue)
        {
            long bitTour = plateau.bitboards[((int)Piece.TOUR_BLANCHE + pointDeVue)];
            if (bitTour == 0)
                return 0;

            int score = 0;
            for (int casePlateau = 0; casePlateau < Plateau.NB_CASES; casePlateau++)
            {
                // On trouve une tour
                if ((bitTour & Plateau.BitsCases[casePlateau]) != 0)
                {
                    // Est ce que la tour est sur la 7ème ligne ?
                    int ligne = (casePlateau >> 3);
                    int colomne = (casePlateau % 8);
                    if ((pointDeVue == Joueur.COTE_BLANC) && (ligne == 1))
                        score += 22;
                    if ((pointDeVue == Joueur.COTE_NOIR) && (ligne == 7))
                        score += 22;
                    /*
                    // Is this rook on a semi- or completely open file?
                    if (MaxPawnFileBins[file] == 0)
                    {
                        if (MinPawnFileBins[file] == 0)
                            score += 10;
                        else
                            score += 4;
                    }*/
                    /*
                    // Is this rook behind a passed pawn?
                    if ((fromWhosePerspective == jcPlayer.SIDE_WHITE) &&
                         (MaxPassedPawns[file] < square))
                        score += 25;
                    if ((fromWhosePerspective == jcPlayer.SIDE_BLACK) &&
                         (MaxPassedPawns[file] > square))
                        score += 25;
                    */
                    // A chaque fois que l'on évalue une tour on l'enlève comme ca on peut gagner du temps à la fin
                    bitTour ^= Plateau.BitsCases[casePlateau];
                    if (bitTour == 0)
                        break;
                }
            }
            return score;
        }

        public int evaluationComplete(Plateau p, int pointDeVue)
        {
            return ((p.evalMateriel(pointDeVue) + EvalDeveloppement(p, pointDeVue) + evalTourBonus(p,pointDeVue) >> Grain) << Grain);
        }

    }
}
