﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teachesser.Model
{
    /// <summary>
    /// Classe représentant une entrée dans la table de transposition
    /// evaluation correspond à la valeur de la position précédemment calculée
    /// la profondeur correspond à la profondeur à la quelle a été trouvé l'éval
    /// Le verrou sert pour les collisions
    /// la date permet de virer des positions qui ne seraient plus possible
    /// </summary>
    class EntreeTableTransposition
    {

        public int typeEvaluation;
        public int evaluation;
        public int profondeur;
        public long verrou;
        public int date;
        // permet de voir si l'entree est déja utilisé ou non
        public static readonly int NULL_ENTREE = -1;

        public EntreeTableTransposition()
        {
            typeEvaluation = NULL_ENTREE;
        }

    }
    class TableTransposition
    {
        //Pq cette taille ?
        private static readonly int TAILLE_TABLE = 131072;

        private EntreeTableTransposition[] Table;
        //Instanciation de toutes les entrées de la table;
        public TableTransposition()
        {
            Table = new EntreeTableTransposition[TAILLE_TABLE];
            for (int i = 0; i < TAILLE_TABLE; i++)
		    {
                Table[i] = new EntreeTableTransposition();
		    }
        }
        /// <summary>
        /// Permet de voir si la situation existe déjà dans la table de Transpo
        /// </summary>
        /// <param name="plateau">Le plateau que l'on veut vérifier</param>
        /// <param name="dep">Renvoie les valeurs dans dep si il y a un match</param>
        /// <returns>renvoie true s'il y a un match avec les valeurs dans le dep sinon false</returns>
        public Deplacement chercherPlateau(Plateau plateau)
        {
            Deplacement dep = new Deplacement();
            //On récupère la valeur du hash du plateau 
            //Valeur absolue afin de l'utiliser comme indice de tableau
            int key = Math.Abs(
                plateau.HashKey() % TAILLE_TABLE);
            // On recupere l'entrée donnée pour le hash
            EntreeTableTransposition entree = Table[key];
            // Si l'entrée n'a pas été évalué alors elle ne nous sert à rien
            if (entree.typeEvaluation == -1)
                return null;
            // Si le verrou de l'entrée est différent de celui du plateau alors on a eu une
            // collision
            if (entree.verrou != plateau.HashLock())
                return null;

            //C'est bon l'entrée existe déjà on la renvoie dans le déplacement voulu et true
            dep.evaluationDeplacement = entree.evaluation;
            dep.profondeurRecherche = entree.profondeur;
            dep.typeEvaluationDeplacement = entree.typeEvaluation;
            return dep;
        }
        public bool sauvegarderPlateau(Plateau plateau, int eval, int evalType, int profondeur, int date)
        {
            int key = Math.Abs(plateau.HashKey() % TAILLE_TABLE);

            if ((Table[key].typeEvaluation != EntreeTableTransposition.NULL_ENTREE) &&
                (Table[key].profondeur >= profondeur) &&
                (Table[key].date >= date))
                return true;

            Table[key].date = date;
            Table[key].evaluation = eval;
            Table[key].profondeur = profondeur;
            Table[key].typeEvaluation = evalType;
            Table[key].verrou = plateau.HashLock();
            return true;
        }
 

    }
}
