﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teachesser.Model
{
    /// <summary>
    /// Enumération qui atribue un numéro à chaque pièce
    /// Constantes PE BitBoard
    /// Pieces relatives aux bitboards
    /// </summary>
    public enum Piece
    {  
        CASE_VIDE       = -1,
        PION_BLANC      = 0,
        PION_NOIR       = 1,
        CAVALIER_BLANC  = 2,
        CAVALIER_NOIR   = 3,
        FOU_BLANC       = 4,
        FOU_NOIR        = 5,
        TOUR_BLANCHE    = 6,
        TOUR_NOIRE      = 7,
        DAME_BLANCHE    = 8,
        DAME_NOIRE      = 9,
        ROI_BLANC       = 10,
        ROI_NOIR        = 11,
        PIECES_BLANCHES = 12,
        PIECES_NOIRES   = 13

        
    }
    static class methodesPourEnumPiece
    {
        public static string getString(this Piece piece)
        {
            switch (piece)
            {
                case Piece.PION_BLANC:
                    return "PB";
                case Piece.PION_NOIR:
                    return "PN";
                case Piece.CAVALIER_BLANC:
                    return "CB";
                case Piece.CAVALIER_NOIR:
                    return "CN";
                case Piece.FOU_BLANC:
                    return "FB";
                case Piece.FOU_NOIR:
                    return "FN";
                case Piece.TOUR_BLANCHE:
                    return "TB";
                case Piece.TOUR_NOIRE:
                    return "TN";
                case Piece.DAME_BLANCHE:
                    return "DB";
                case Piece.DAME_NOIRE:
                    return "DN";
                case Piece.ROI_BLANC:
                    return "RB";
                case Piece.ROI_NOIR:
                    return "RN";
                case Piece.CASE_VIDE:
                    return "CV";
                

            }
            return " ";
        }
    }

}
