﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teachesser.Model
{
    /// <summary>
    /// Classe représentant une entrée dans la table d'ouverture avec deux déplacements
    /// et inspiré d'une entrée de tableTransposition
    /// </summary>
    class EntreeTableOuverture
    {
        public int verrou;
        public Deplacement DeplacementBlanc;
        public Deplacement DeplacementNoir;

        public static readonly int NO_MOVE = -1;
        public EntreeTableOuverture()
        {
            verrou = 0;
            DeplacementBlanc = new Deplacement();
            DeplacementNoir = new Deplacement();
            DeplacementBlanc.typeDeplacement = TypeDeplacement.NULL_MOVE;
            DeplacementNoir.typeDeplacement = TypeDeplacement.NULL_MOVE;
        }
    }
    class TableOuverture
    {
        // taille de la table on peut la changer
        static readonly int TAILLE_TABLE = 1024;
        EntreeTableOuverture[] Table = new EntreeTableOuverture[TAILLE_TABLE];
        

        public TableOuverture()
        {
            for (int i = 0; i < TAILLE_TABLE; i++)
            {
                Table[i] = new EntreeTableOuverture();

            }
        }
        /// <summary>
        /// permet de vérifier s'il n'y a pas un coup qui existe déja dans la table d'ouverture
        /// si oui renvoie un déplacement
        /// </summary>
        /// <param name="plateau">le plateau du jeu</param>
        /// <returns>le déplacement possible null sinon</returns>
        public Deplacement requete(Plateau plateau)
        {
            int cle = Math.Abs(plateau.HashKey() % TAILLE_TABLE);
            int verrou = plateau.HashLock();

            if (Table[cle].verrou != verrou)
                return null;

            /*if (plateau.joueurCourant == Joueur.COTE_NOIR)
            {
                if (Table[cle].DeplacementNoir.typeDeplacement != EntreeTableOuverture.NO_MOVE)
                    return Table[cle].DeplacementNoir;
            }
            else
            {
                if (Table[cle].DeplacementBlanc.typeDeplacement != EntreeTableOuverture.NO_MOVE)
                    return Table[cle].DeplacementBlanc;
            }*/

            return null;
        }
       
                





    }
}
