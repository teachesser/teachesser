﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teachesser.Model
{
    public abstract class Joueur
    {
        public static readonly int COTE_NOIR  = 1;
        public static readonly int COTE_BLANC = 0;

        // Utiles pour la partie
        private int _couleur;

        #region GETTER et SETTER
        Deplacement getDeplacement(Plateau plateau)
        {
            return null;
        }

        public int Couleur 
        {
             get
            {
                return _couleur;
            }
            set
            {
                _couleur = value;
            }
        }
        #endregion
    }
}
