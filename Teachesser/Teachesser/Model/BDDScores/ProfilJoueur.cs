﻿using System;
using System.Collections.ObjectModel;
using Teachesser.Model.BDDScores;

/// <summary>
/// Profil du joueur (à ne pas confondre avec le joueur de la partie)
/// Utilisé pour la BDD
/// </summary>
/// <author> Raphaël Turquet </author>
namespace Teachesser.Model
{
    public class ProfilJoueur
    {
        private String pseudo;
        private Int32 _nbMatch;
        private int _nbVictoire;
        private int _nbNul;
        private int _nbDef;

        //Pour créer un joueur
        public ProfilJoueur(){}

        //Pour créer un joueur
        public ProfilJoueur(String pseudo)
        {
            this.pseudo = pseudo;
            this.NbMatch = 0;
            this.NbVictoire = 0;
            this.NbNul = 0;
            this.NbDef = 0;
        }

        #region Fonctions
        //Pour récuperer un joueur depuis la BDD
        public ProfilJoueur(String s, int nbMatch, int nbVictoire, int nbNul)
        {
            this.pseudo         = s;
            this._nbMatch       = nbMatch;
            this._nbVictoire    = nbVictoire;
            this._nbNul         = nbNul;
            this._nbDef         = _nbMatch - (_nbVictoire + _nbNul);
        }

        /// <summary>
        /// Fonction qui insert un joueur dans la BDD
        /// </summary>
        /// <returns>execution</returns>
        public bool InsererJoueur()
        {
            bool execution = GestionnaireBDD.WriteXML(this);

            return execution;
        }
        #endregion

        #region GETTERS SETTERS

        public String Pseudo
        {
            get{return pseudo;}
            set{pseudo = value;}
        }

        public int NbMatch
        {
            get{return _nbMatch;}
            set{_nbMatch = value;}
        }

        public int NbVictoire
        {
            get{return _nbVictoire;}
            set{_nbVictoire = value;}
        }

        public int NbNul
        {
            get{return _nbNul;}
            set{_nbNul = value;}
        }

        public int NbDef
        {
           get{return _nbDef;}
           set{_nbDef = value;}
        }

        #endregion
    }
}
