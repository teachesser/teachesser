﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using System.IO;

namespace Teachesser.Model.BDDScores
{
    /// <summary>
    /// Classe qui gere les accès en ecriture 
    /// et en lecture à la BDD de scores
    /// </summary>
    public class GestionnaireBDD
    {
        static string filePath = @"..\..\Model\BDDScores\BDDScores.xml";

        /// <summary>
        /// Fonction qui retourne l'ensemble des joueurs 
        /// contenus dans la BDD XML
        /// </summary>
        /// <returns>Liste des joueurs</returns>
        public static ObservableCollection<ProfilJoueur> ReadXML()
        {
            ObservableCollection<ProfilJoueur> oC = new ObservableCollection<ProfilJoueur>();
            XmlDocument xmlDoc                    = new XmlDocument(); xmlDoc.Load(filePath);
            XmlNodeList nodeList                  = xmlDoc.DocumentElement.SelectNodes("/Joueurs/Joueur");

            //Parcours d'un noeud Joueur
            //Récupération des informations
            foreach (XmlNode node in nodeList)
            {
                ProfilJoueur pF = new ProfilJoueur();
                pF.Pseudo       = node.SelectSingleNode("Pseudonyme").InnerText;
                pF.NbMatch      = Int32.Parse(node.SelectSingleNode("Matches").InnerText);
                pF.NbVictoire   = Int32.Parse(node.SelectSingleNode("Victoires").InnerText);
                pF.NbNul        = Int32.Parse(node.SelectSingleNode("Pats").InnerText);
                pF.NbDef        = pF.NbMatch - (pF.NbNul + pF.NbVictoire);
                oC.Add(pF);
            }

            return oC; 
        }


        /// <summary>
        /// Fonction qui ecrit un nouveau profil de joueur 
        /// </summary>
        /// <param name="pf"></param>
        /// <returns></returns>
        public static Boolean WriteXML(ProfilJoueur pf)
        {
            Boolean b = false;
            bool existe = GestionnaireBDD.TestPseudoExiste(pf.Pseudo);  // Test si le pseudo existe déjà dans la BDD

            // Si le pseudo n'existe pas déjà
            if (existe == false)
            {
                XmlDocument xmlDoc  = new XmlDocument(); xmlDoc.Load(filePath);
                XmlNode rootNode    = xmlDoc.SelectSingleNode("//Joueurs");

                //Création des éléments
                XmlElement joueur   = xmlDoc.CreateElement("Joueur");
                XmlElement pseudo   = xmlDoc.CreateElement("Pseudonyme");
                XmlElement victoire = xmlDoc.CreateElement("Victoires");
                XmlElement pat      = xmlDoc.CreateElement("Pats");
                XmlElement match    = xmlDoc.CreateElement("Matches");

                //Ecriture du content
                pseudo.InnerText    = pf.Pseudo;
                victoire.InnerText  = pf.NbVictoire.ToString();
                pat.InnerText       = pf.NbNul.ToString();
                match.InnerText     = pf.NbMatch.ToString();

                //Ajout des éléments aux xml
                rootNode.AppendChild(joueur);
                joueur.AppendChild(pseudo);
                joueur.AppendChild(victoire);
                joueur.AppendChild(pat);
                joueur.AppendChild(match);

                //Enregistrement des modifications
                xmlDoc.Save(filePath);
                b = true;
            }
            return b;
        }

        /// <summary>
        /// Fonction qui test si un joueur existe déjà dans la BDD
        /// </summary>
        /// <param name="pf"></param>
        /// <returns></returns>
        public static bool TestPseudoExiste(String pseudo)
        {
            bool existe = false;
            ObservableCollection<ProfilJoueur> data = GestionnaireBDD.ReadXML();

            for (int i = 0; i < data.Count(); i++)
            {
                // Cas où le pseudo existe
                if(data[i].Pseudo.ToUpper().CompareTo(pseudo.ToUpper()) == 0)
                {
                    return true;
                }
            }

            return existe;
        }

        /// <summary>
        /// Fonction réinitialise la BDD des scores
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public void ReinitialisationScore()
        {
            XmlDocument xmlDoc = new XmlDocument(); xmlDoc.Load(filePath);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Joueurs");

            //Parcours des Joueur et suppression de ceux-ci
            foreach (XmlNode node in nodeList)
            {
                node.RemoveAll();
            }

            //Enregistrement des modifications
            xmlDoc.Save(filePath);
        }
/*
        public void ChangerScore(JoueurConcret gagnant, JoueurConcret perdant)
        {
            XmlDocument xmlDoc = new XmlDocument(); xmlDoc.Load(filePath);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/Joueurs/Joueur");

            foreach (XmlNode node in nodeList)
            {
                if (gagnant.PF.Pseudo.Equals(node.SelectSingleNode("Pseudonyme").InnerText))
                {
                    XmlElement element =  node.SelectSingleNode("Matches");
                    int nbMatch = Int32.Parse(node.InnerText);
                    node.InnerText
                    gagnant.PF.NbVictoire++;
                    gagnant.PF.NbMatch++;
                }

                pF.Pseudo = node.SelectSingleNode("Pseudonyme").InnerText;
                pF.NbMatch = Int32.Parse(node.SelectSingleNode("Matches").InnerText);
                pF.NbVictoire = Int32.Parse(node.SelectSingleNode("Victoires").InnerText);
                pF.NbNul = Int32.Parse(node.SelectSingleNode("Pats").InnerText);
                pF.NbDef = pF.NbMatch - (pF.NbNul + pF.NbVictoire);

                
                
            }

        }
*/
    }
}  
