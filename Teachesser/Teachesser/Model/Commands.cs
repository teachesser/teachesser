﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using Teachesser.Model;
using System.Collections;
using System.Collections.ObjectModel;
using Teachesser.Model.IntelligenceArtificielle;

namespace Teachesser
{
    public class Commands : ICommand
    {
        public static readonly RoutedUICommand deplacementIA = new RoutedUICommand("deplacementIA", "deplacementIA", typeof(Commands));
        public static readonly RoutedUICommand finDeTour = new RoutedUICommand("finDeTour", "finDeTour", typeof(Commands));
        public static readonly RoutedUICommand options = new RoutedUICommand("options", "options", typeof(Commands));
        
        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            throw new NotImplementedException();
        }
    }
}
