﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Teachesser.Model.BDDScores;

namespace Teachesser.Model.Partie
{
    /// <summary>
    /// Classe qui modélise les informations d'une partie
    /// </summary>
    public class Partie
    {
        private JoueurConcret _joueurBlanc = new JoueurConcret(new ProfilJoueur(), Joueur.COTE_BLANC);
        private JoueurConcret _joueurNoir = new JoueurConcret(new ProfilJoueur(), Joueur.COTE_NOIR);

        //Le typage de ces deux variables n'est pas définitif
        private int _tempsDeJeu;
        private int _mode;

        #region CONSTRUCTEURS
        public Partie(){}

        public Partie(int tdp, int mode)
        {
            this._tempsDeJeu = tdp;
            this._mode       = mode;
        }

        public Partie(ProfilJoueur joueur1, ProfilJoueur joueur2, int tdp, int mode)
        {
            this._tempsDeJeu  = tdp;
            this._mode        = mode; 
        }

        
        #endregion

        #region GETTERS ET SETTERS
        public JoueurConcret JoueurBlanc
        {
            get { return _joueurBlanc; }
            set { _joueurBlanc = value;}
        }

        public JoueurConcret JoueurNoir
        {
            get { return _joueurNoir; }
            set { _joueurNoir = value;}
        }

        public int TempsDeJeu
        {
            get { return _tempsDeJeu; }
            set { _tempsDeJeu = value;}
        }

        public int Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
        #endregion
    }
}
