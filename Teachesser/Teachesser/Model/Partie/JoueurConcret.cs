﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teachesser.Model
{
    /// <summary>
    /// Classe qui associe profil de Joueur et une couleur
    /// Utiliser lors de la création de la partie
    /// </summary>
 
    public class JoueurConcret:Joueur
    {
        private ProfilJoueur _pF;
        private bool _pret = false;

        public JoueurConcret(ProfilJoueur pF, int couleur)
        {
            this.PF = pF;
            this.Couleur = couleur;
        }

        #region GETTERS ET SETTERS
        public ProfilJoueur PF
        {
            get{return _pF;}
            set{_pF = value;}
        }

        public bool Pret
        {
            get { return _pret ;}
            set { _pret = value;}
        }
        #endregion
    }
}
