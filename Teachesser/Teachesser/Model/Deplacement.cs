﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Media;

namespace Teachesser.Model
{
    public enum TypeDeplacement
    {
        DEPLACEMENT_NORMAL = 0,
        DEPLACEMENT_CAPTURE_ORDINAIRE = 1,
        DEPLACEMENT_CAPTURE_EN_PASSANT = 2,
        DEPLACEMENT_PETIT_ROQUE = 4,
        DEPLACEMENT_GRAND_ROQUE = 8,
        DEPLACEMENT_ABANDON = 16,
        DEPLACEMENT_PAT = 17,
        DEPLACEMENT_ILLEGAL_ECHEC = 18,
        NULL_MOVE = -1,
    }

    public enum TypeEvaluationDeplacement
    {
        EVALUATION_PRECISE = 0,
        EVALUATION_MAJORANTE = 1,
        EVALUATION_MINORANTE = 2,
        
        NULL_MOVE = -1,
    }

    public class Deplacement
    {
        public int evaluationDeplacement;
        public int typeEvaluationDeplacement;
        public int profondeurRecherche;
        public static readonly int NULL_MOVE = -1;

        // Les différents type de mouvements
        public TypeDeplacement typeDeplacement;
        // S'agit-il d'une promotion (dans le cas d'un pion)
        public bool promotion;
        // La pièce en train de se mouvoir
        public Piece pieceDeplacee;
        // La pièce possiblement capturee
        public Piece pieceCapturee;
        // Les cases correspondantes au mouvement
        public Case caseSource, caseDestination;

        // Constantes pour les roques
        private static readonly int PETIT_ROQUE = 1;
        private static readonly int GRAND_ROQUE = 2;

        // Type de mouvement possible
        public Deplacement()
        {
            pieceDeplacee = Piece.CASE_VIDE;
            pieceCapturee = Piece.CASE_VIDE;
            caseSource = Case.NULL_CASE;
            caseDestination = Case.NULL_CASE;
            typeDeplacement = TypeDeplacement.NULL_MOVE;
            evaluationDeplacement = NULL_MOVE;
            typeEvaluationDeplacement = NULL_MOVE;
            profondeurRecherche = NULL_MOVE;
        }

        /// <summary>
        /// Constructeur standard pour un déplacement normal
        /// </summary>
        /// <param name="caseSource"></param>
        /// <param name="caseDestination"></param>
        public Deplacement(Case caseSource, Case caseDestination, Piece pQuiBouge)
        {
            pieceDeplacee = pQuiBouge;
            pieceCapturee = Piece.CASE_VIDE;
            this.caseSource = caseSource;
            this.caseDestination = caseDestination;
            typeDeplacement = TypeDeplacement.DEPLACEMENT_NORMAL;
            promotion = false;
            evaluationDeplacement = NULL_MOVE;
            typeEvaluationDeplacement = NULL_MOVE;
            profondeurRecherche = NULL_MOVE;
        }

        /// <summary>
	    /// Méthode qui permet d'ajouter un deplacement et qui renvoie un boolen pour savoir si on doit encore chercher
        /// </summary>
        /// <param name="piece">Piece concernée par le déplacement</param>
        /// <param name="caseDestination">La case ou se déplace la pièce</param>
        /// <param name="caseSource">La case d'ou vient la pièce</param>
        /// <param name="casesPossibles"></param>
        /// <returns>Détermine s'il on continue à chercher dans la direction</returns>
        private static bool ajoutDeplacementPossible(Plateau plateau, Piece piece, Case caseDestination, Case caseSource, ref List<Deplacement> deplacementsPossibles)
        {
            Deplacement dep = new Deplacement(caseSource, caseDestination, piece);
            
            Piece pieceCapturee = plateau.trouverPiece(caseDestination);
            // S'il y a une piece adverse sur la case
            //if (pieceCapturee != Piece.CASE_VIDE && (((int)pieceCapturee % 2) != ((int) piece % 2)))
            if ((plateau.bitboards[(int)Piece.PIECES_NOIRES - ((int)piece % 2)] & Plateau.BitsCases[(int)caseDestination]) != 0)
            {
                // On joue le coup pour verifier s'il laisse le roi en echec
                plateau.retirerPiece(caseSource, piece);
                plateau.retirerPiece(caseDestination, pieceCapturee);
                plateau.ajouterPiece(caseDestination, piece);

                // Si le deplacement laisse le roi allie en echec
                if (plateau.estEnEchec(Piece.ROI_BLANC + (int)piece % 2))
                {
                    // On declare le deplacement comme illegal
                    dep.typeDeplacement = TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC;
                }
                else
                {
                    // On declare le deplacement comme une capture ordinaire
                    dep.typeDeplacement = TypeDeplacement.DEPLACEMENT_CAPTURE_ORDINAIRE;
                    dep.pieceCapturee = pieceCapturee;
                }

                // On repositionne le plateau dans son etat initial
                plateau.retirerPiece(caseDestination, piece);
                plateau.ajouterPiece(caseSource, piece);
                plateau.ajouterPiece(caseDestination, pieceCapturee);

                // Test promotion des pions
                if ((piece == Piece.PION_BLANC || piece == Piece.PION_NOIR) && (caseDestination >= Case.A1 && caseDestination <= Case.H1)
                    || (caseDestination >= Case.A8 && caseDestination <= Case.H8))
                {
                    dep.promotion = true;
                }

                // Ajout du deplacement a la liste
                deplacementsPossibles.Add(dep);
                // On arrete de chercher dans cette direction
                return false;
            }
            // S'il y a une piece alliee sur la case
            //else if (pieceCapturee != Piece.CASE_VIDE && (((int)pieceCapturee % 2) == ((int) piece % 2)))
            else if ((plateau.bitboards[(int)Piece.PIECES_BLANCHES + ((int)piece % 2)] & Plateau.BitsCases[(int)caseDestination]) != 0)
            {
                // On arrete de chercher dans cette direction
                return false;
            }

            // Sinon, ajout de la case a la liste et poursuite de la recherche
            // On verifie tout de meme si le deplacement laisse le roi en echec
            plateau.retirerPiece(caseSource, piece);
            plateau.ajouterPiece(caseDestination, piece);

            if (plateau.estEnEchec(Piece.ROI_BLANC + (int)piece % 2))
            {
                dep.typeDeplacement = TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC;
            }

            plateau.retirerPiece(caseDestination, piece);
            plateau.ajouterPiece(caseSource, piece);

            deplacementsPossibles.Add(dep);
            return true;
        }

        private static bool ajoutDeplacementPion(Plateau plateau, Piece piece, Case caseDestination, Case caseSource, ref List<Deplacement> deplacementsPossibles)
        {
            Deplacement dep = new Deplacement(caseSource, caseDestination, piece);

            // S'il n'y a pas de piece sur la case
            if (((plateau.bitboards[(int)Piece.PIECES_BLANCHES] | plateau.bitboards[(int)Piece.PIECES_NOIRES]) & Plateau.BitsCases[(int)caseDestination]) == 0)
            {
                // On joue le coup pour verifier s'il laisse le roi en echec
                plateau.retirerPiece(caseSource, piece);
                plateau.ajouterPiece(caseDestination, piece);

                // Si le deplacement laisse le roi allie en echec
                if (plateau.estEnEchec(Piece.ROI_BLANC + (int)piece % 2))
                {
                    // On declare le deplacement comme illegal
                    dep.typeDeplacement = TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC;
                }
                else
                {
                    // On declare le deplacement comme un deplacement normal
                    dep.typeDeplacement = TypeDeplacement.DEPLACEMENT_NORMAL;
                }

                // On repositionne le plateau dans son etat initial
                plateau.retirerPiece(caseDestination, piece);
                plateau.ajouterPiece(caseSource, piece);

                // Test promotion
                if ((caseDestination >= Case.A1 && caseDestination <= Case.H1) || (caseDestination >= Case.A8 && caseDestination <= Case.H8))
                {
                    dep.promotion = true;
                }

                // Ajout du deplacement a la liste
                deplacementsPossibles.Add(dep);

                return true;
            }
            return false;
        }

        private static bool ajoutCasePossible(Plateau plateau, Piece piece, Case caseDestination, ref long coupsPossibles)
        {
            // S'il y a une piece adverse sur la case
            
            if ((plateau.bitboards[(int)Piece.PIECES_NOIRES - ((int)piece % 2)] & Plateau.BitsCases[(int)caseDestination]) != 0)
            {
                // Ajout de la case
                coupsPossibles |= Plateau.BitsCases[(int)caseDestination];
                // On arrete de chercher dans cette direction
                return false;
            }
            // S'il y a une piece alliee sur la case
            else if ((plateau.bitboards[(int)Piece.PIECES_BLANCHES + ((int)piece % 2)] & Plateau.BitsCases[(int)caseDestination]) != 0)
            {
                // On arrete de chercher dans cette direction
                return false;
            }

            // Sinon, ajout de la case a la liste et poursuite de la recherche
            coupsPossibles |= Plateau.BitsCases[(int)caseDestination];
            return true;
        }

        public static List<Deplacement> CoupsPossiblesTour(Case indexCase , Plateau plateau)
        {
            List<Deplacement> cases = new List<Deplacement>();
            Case i = indexCase;
            Piece piece = plateau.trouverPiece(indexCase);

            // Recherche vers le nord
            i -= 8;
            while (i >= Case.A8 && ajoutDeplacementPossible(plateau, piece, i, indexCase, ref cases))
            {
                i -= 8;
            }

            // Recherche vers l'est
            i = indexCase + 1;
            while (i <= Case.H1 && (int)i % 8 > 0 && ajoutDeplacementPossible(plateau, piece, i, indexCase, ref cases))
            {
                i++;
            }

            // Recherche vers le sud
            i = indexCase + 8;
            while (i <= Case.H1 && ajoutDeplacementPossible(plateau, piece, i, indexCase, ref cases))
            {
                i += 8;
            }
            
            // Recherche vers l'ouest
            i = indexCase - 1;
            while (i >= Case.A8 && (int)i % 8 < 7 && ajoutDeplacementPossible(plateau, piece, i, indexCase, ref cases))
            {
                i--;
            }

            return cases;
        }

        public static long CasesPossiblesTour(Plateau plateau,Case indexCase)
        {
            long cases = 0;
            Case i = indexCase;
            Piece piece = plateau.trouverPiece(indexCase);

            // Recherche vers le nord
            i -= 8;
            while (i >= Case.A8 && ajoutCasePossible(plateau, piece, i, ref cases))
            {
                i -= 8;
            }

            // Recherche vers l'est
            i = indexCase + 1;
            while (i <= Case.H1 && (int)i % 8 > 0 && ajoutCasePossible(plateau, piece, i, ref cases))
            {
                i++;
            }

            // Recherche vers le sud
            i = indexCase + 8;
            while (i <= Case.H1 && ajoutCasePossible(plateau, piece, i, ref cases))
            {
                i += 8;
            }

            // Recherche vers l'ouest
            i = indexCase - 1;
            while (i >= Case.A8 && (int)i % 8 < 7 && ajoutCasePossible(plateau, piece, i, ref cases))
            {
                i--;
            }

            return cases;
        }

        public static List<Deplacement> CoupsPossiblesCavalier(Plateau p, Case indexCase)
        {
            List<Deplacement> cases = new List<Deplacement>();
            
            Piece piece = p.trouverPiece(indexCase);

            if (indexCase >= Case.A7 && (int)indexCase % 8 <= 5)
            {
                ajoutDeplacementPossible(p, piece, indexCase - 6, indexCase, ref cases);
            }

            if (indexCase >= Case.C7 && (int)indexCase % 8 >= 2)
            {
                ajoutDeplacementPossible(p, piece, indexCase - 10, indexCase, ref cases);
            }

            if (indexCase >= Case.A6 && (int)indexCase % 8 <= 6)
            {
                ajoutDeplacementPossible(p, piece, indexCase - 15, indexCase, ref cases);
            }

            if (indexCase >= Case.B6 && (int)indexCase % 8 >= 1)
            {
                ajoutDeplacementPossible(p, piece, indexCase - 17, indexCase, ref cases);
            }

            if (indexCase <= Case.H2 && (int)indexCase % 8 >= 2)
            {
                ajoutDeplacementPossible(p, piece, indexCase + 6, indexCase, ref cases);
            }

            if (indexCase <= Case.F2 && (int)indexCase % 8 <= 5)
            {
                ajoutDeplacementPossible(p, piece, indexCase + 10, indexCase, ref cases);
            }

            if (indexCase <= Case.H3 && (int)indexCase % 8 >= 1)
            {
                ajoutDeplacementPossible(p, piece, indexCase + 15, indexCase, ref cases);
            }

            if (indexCase <= Case.G3 && (int)indexCase % 8 <= 6)
            {
                ajoutDeplacementPossible(p, piece, indexCase + 17, indexCase, ref cases);
            }

            return cases;
        }

        public static long CasesPossiblesCavalier(Plateau p,Case indexCase)
        {
            long cases = 0;
            Piece piece = p.trouverPiece(indexCase);

            if (indexCase >= Case.A7 && (int)indexCase % 8 <= 5)
            {
                ajoutCasePossible(p, piece, indexCase - 6, ref cases);
            }

            if (indexCase >= Case.C7 && (int)indexCase % 8 >= 2)
            {
                ajoutCasePossible(p, piece, indexCase - 10, ref cases);
            }

            if (indexCase >= Case.A6 && (int)indexCase % 8 <= 6)
            {
                ajoutCasePossible(p, piece, indexCase - 15, ref cases);
            }

            if (indexCase >= Case.B6 && (int)indexCase % 8 >= 1)
            {
                ajoutCasePossible(p, piece, indexCase - 17, ref cases);
            }

            if (indexCase <= Case.H2 && (int)indexCase % 8 >= 2)
            {
                ajoutCasePossible(p, piece, indexCase + 6, ref cases);
            }

            if (indexCase <= Case.F2 && (int)indexCase % 8 <= 5)
            {
                ajoutCasePossible(p, piece, indexCase + 10, ref cases);
            }

            if (indexCase <= Case.H3 && (int)indexCase % 8 >= 1)
            {
                ajoutCasePossible(p, piece, indexCase + 15, ref cases);
            }

            if (indexCase <= Case.G3 && (int)indexCase % 8 <= 6)
            {
                ajoutCasePossible(p, piece, indexCase + 17, ref cases);
            }

            return cases;
        }

        public static List<Deplacement> CoupsPossiblesFou(Plateau p,Case indexCase)
        {
            List<Deplacement> cases = new List<Deplacement>();
            Case i = indexCase;
            Piece piece = p.trouverPiece(indexCase);
            
            // Recherche vers le nord-ouest
            i -= 9;
            while (i >= Case.A8 && (int)i % 8 != 7 && ajoutDeplacementPossible(p, piece, i, indexCase, ref cases))
            {
                i -= 9;
            }

            // Recherche vers le nord-est
            i = indexCase - 7;
            while (i >= Case.A8 && (int)i % 8 != 0 && ajoutDeplacementPossible(p, piece, i, indexCase, ref cases))
            {
                i -= 7;
            }

            // Recherche vers le sud-est
            i = indexCase + 9;
            while (i <= Case.H1 && ((int)i % 8 != 0) && ajoutDeplacementPossible(p, piece, i, indexCase, ref cases))
            {
                i += 9;
            }

            // Recherche vers le sud-ouest
            i = indexCase + 7;
            while (i < Case.H1 && ((int)i % 8 != 7) && ajoutDeplacementPossible(p, piece, i, indexCase, ref cases))
            {
                i += 7;
            }

            return cases;
        }

        public static long CasesPossiblesFou(Plateau p,Case indexCase)
        {
            long cases = 0;
            Case i = indexCase;
            Piece piece = p.trouverPiece(indexCase);

            // Recherche vers le nord-ouest
            i -= 9;
            while (i >= Case.A8 && (int)i % 8 != 7 && ajoutCasePossible(p, piece, i, ref cases))
            {
                i -= 9;
            }

            // Recherche vers le nord-est
            i = indexCase - 7;
            while (i >= Case.A8 && (int)i % 8 != 0 && ajoutCasePossible(p, piece, i, ref cases))
            {
                i -= 7;
            }

            // Recherche vers le sud-est
            i = indexCase + 9;
            while (i <= Case.H1 && ((int)i % 8 != 0) && ajoutCasePossible(p, piece, i, ref cases))
            {
                i += 9;
            }

            // Recherche vers le sud-ouest
            i = indexCase + 7;
            while (i < Case.H1 && ((int)i % 8 != 7) && ajoutCasePossible(p, piece, i, ref cases))
            {
                i += 7;
            }

            return cases;
        }

        public static List<Deplacement> CoupsPossiblesRoi(Plateau p,Case indexCase)
        {
            List<Deplacement> cases = new List<Deplacement>();
            Case i = indexCase;
            Piece piece = p.trouverPiece(indexCase);
            

            // Recherche vers l'est
            i = indexCase + 1;
            if ((int)i % 8 != 0 && i <=Case.H1 )
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            

            // Recherche vers l'ouest
            i = indexCase - 1;
            if ((int)i % 8 != 7 && i >= Case.A8)
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            
            // Recherche vers le nord
            i = indexCase - 8;
            if ((int)i >= (int)Case.A8)
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            
            // Recherche vers le sud
            i = indexCase + 8;
            if ((int)i <= (int)Case.H1)
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            
            // Recherche vers le nord-ouest
            i = indexCase - 9;
            if (i >= Case.A8 && ((int)i % 8 != 7))
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            
            // Recherche vers le nord-est
            i = indexCase - 7;
            if (i >= Case.A8 && (int)i % 8 != 0)
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            } 
            

            // Recherche vers le sud-est
            i = indexCase + 9;
            if (i <= Case.H1 && ((int)i % 8 != 0))
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            
            // Recherche vers le sud-ouest
            i = indexCase + 7;
            if (i < Case.H1 && ((int)i % 8 != 7))
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            
            // Recherche des roques possibles
            // Petit roque
            if (peutRoquer(p, indexCase, piece, PETIT_ROQUE))
            {
                //Console.WriteLine("Peut petit roque");
                
                Deplacement petitRoque = new Deplacement(indexCase, indexCase + 2, piece);
                petitRoque.typeDeplacement = TypeDeplacement.DEPLACEMENT_PETIT_ROQUE;
                cases.Add(petitRoque);
            }
            
            // Grand roque
            if (peutRoquer(p, indexCase, piece, GRAND_ROQUE))
            {
                //Console.WriteLine("Peut grand roque");
                Deplacement grandRoque = new Deplacement(indexCase, indexCase - 2, piece);
                grandRoque.typeDeplacement = TypeDeplacement.DEPLACEMENT_GRAND_ROQUE;
                cases.Add(grandRoque);
            }
            

            return cases;
        }

        public static long CasesPossiblesRoi(Plateau p, Case indexCase)
        {
            long cases = 0;
            Case i = indexCase;
            Piece piece = p.trouverPiece(indexCase);

            // Recherche vers l'est
            i = indexCase + 1;
            if ((int)i % 8 != 0 && i<=Case.H1)
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            // Recherche vers l'ouest
            i = indexCase - 1;
            if ((int)i % 8 != 7 && i>=Case.A8)
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            // Recherche vers le nord
            i = indexCase - 8;
            if ((int)i >= (int)Case.A8)
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            // Recherche vers le sud
            i = indexCase + 8;
            if ((int)i <= (int)Case.H1)
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            // Recherche vers le nord-ouest
            i = indexCase - 9;
            if (i >= Case.A8 && ((int)i % 8 != 7))
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            // Recherche vers le nord-est
            i = indexCase - 7;
            if (i >= Case.A8 && (int)i % 8 != 0)
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            // Recherche vers le sud-est
            i = indexCase + 9;
            if (i <= Case.H1 && ((int)i % 8 != 0))
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            // Recherche vers le sud-ouest
            i = indexCase + 7;
            if (i < Case.H1 && ((int)i % 8 != 7))
            {
                ajoutCasePossible(p, piece, i, ref cases);
            }

            return cases;
        }

        /// <summary>
        /// Fonction permettant de savoir si un roi peut roquer
        /// </summary>
        /// <param name="plateau">Plateau sur lequel tester</param>
        /// <param name="caseRoi">Case sur laquelle se tient le roi</param>
        /// <param name="roi">Le roi sur lequel tester (ROI_BLANC ou ROI_NOIR)</param>
        /// <param name="typeRoque">La constante PETIT_ROQUE ou GRAND_ROQUE de la classe Deplacement</param>
        /// <returns>Retourne vrai si le roi peut effectuer le roque, faux sinon</returns>
        private static bool peutRoquer(Plateau plateau, Case caseRoi, Piece roi, int typeRoque)
        {
            String tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);
            // Petit roque
            if (typeRoque == PETIT_ROQUE)
            {
                // Test des positions initiales
                if (!plateau.peutPetitRoque[0 + (int)roi % 2])
                {
                    tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);
                    return false;
                }
                
                // Test cases inoccupees
                if (!plateau.caseEstLibre(caseRoi + 1) || !plateau.caseEstLibre(caseRoi + 2))
                {
                    tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);
                    return false;
                }

                // Verifie si le roi est en echec sur les cases qu'il traverse
                plateau.retirerPiece(caseRoi, roi);
                plateau.ajouterPiece(caseRoi + 1, roi);
                tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);
                if (plateau.estEnEchec(roi))
                {
                    // Replacement du roi a sa case d'origine
                    plateau.retirerPiece(caseRoi + 1, roi);
                    plateau.ajouterPiece(caseRoi, roi);
                    return false;
                }
                tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);
                plateau.retirerPiece(caseRoi + 1, roi);
                plateau.ajouterPiece(caseRoi + 2, roi);
                tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);
                if (plateau.estEnEchec(roi))
                {
                    // Replacement du roi a sa case d'origine
                    plateau.retirerPiece(caseRoi + 2, roi);
                    plateau.ajouterPiece(caseRoi, roi);
                    return false;
                }
                tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);

                // Replacement du roi a sa case d'origine
                plateau.retirerPiece(caseRoi + 2, roi);
                plateau.ajouterPiece(caseRoi, roi);
                tmp = Convert.ToString(plateau.bitboards[(int)Piece.PIECES_NOIRES], 2);
            }
            // Grand roque
            else
            {
                // Test des positions initiales
                if (!plateau.peutGrandRoque[0 + (int)roi % 2])
                {
                    return false;
                }

                // Test cases inoccupees
                if (!plateau.caseEstLibre(caseRoi - 1) || !plateau.caseEstLibre(caseRoi - 2) || !plateau.caseEstLibre(caseRoi - 3))
                {
                    return false;
                }

                // Verifie si le roi est en echec sur les cases qu'il traverse
                plateau.retirerPiece(caseRoi, roi);
                plateau.ajouterPiece(caseRoi - 1, roi);

                if (plateau.estEnEchec(roi))
                {
                    // Replacement du roi a sa case d'origine
                    plateau.retirerPiece(caseRoi - 1, roi);
                    plateau.ajouterPiece(caseRoi, roi);
                    return false;
                }

                plateau.retirerPiece(caseRoi - 1, roi);
                plateau.ajouterPiece(caseRoi - 2, roi);

                if (plateau.estEnEchec(roi))
                {
                    // Replacement du roi a sa case d'origine
                    plateau.retirerPiece(caseRoi - 2, roi);
                    plateau.ajouterPiece(caseRoi, roi);
                    return false;
                }

                // Replacement du roi a sa case d'origine
                plateau.retirerPiece(caseRoi - 2, roi);
                plateau.ajouterPiece(caseRoi, roi);
            }

            return true;
        }

        public static List<Deplacement> CoupsPossiblesPionBlanc(Plateau p, Case indexCase)
        {
            List<Deplacement> cases = new List<Deplacement>();
            Case i = indexCase;
            Piece piece = Piece.PION_BLANC;
            Deplacement dep = new Deplacement();

            // On regarde si on peut avancer tout droit
            if (i - 8 >= Case.A8)
            {
                bool caseLibre = ajoutDeplacementPion(p, piece, i - 8, indexCase, ref cases);
                // Si le pion est sur la premiere ligne, on teste une case supplementaire
                if ((i >= Case.A2) && (i <= Case.H2) && caseLibre)
                {
                    ajoutDeplacementPion(p, piece, i - 16, indexCase, ref cases);
                }
            }

            // On regarde si on peut prendre des pieces
            i = indexCase - 9;
            if ((int)i % 8 != 7 && i >= Case.A8 && ((p.bitboards[(int)Piece.PIECES_NOIRES] & Plateau.BitsCases[(int)i]) != 0))
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            i = indexCase - 7;
            if ((int)i % 8 != 0 && i >= Case.A8 && ((p.bitboards[(int)Piece.PIECES_NOIRES] & Plateau.BitsCases[(int)i]) != 0))
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }

            // On regarde si une prise en passant est possible
            // Si le pion est sur la 5e ligne
            if (indexCase >= Case.A5 && indexCase <= Case.H5)
            {
                dep = p.dernierDeplacement;
                // Si le dernier deplacement est un pion adverse qui avance de deux cases
                if (dep != null)
                if (dep.typeDeplacement == TypeDeplacement.DEPLACEMENT_NORMAL && dep.pieceDeplacee == Piece.PION_NOIR 
                    && dep.caseSource >= Case.A7 && dep.caseSource <= Case.H7 && dep.caseDestination == dep.caseSource + 16)
                {
                    // Si le pion adverse atterit a gauche du pion courant
                    if (dep.caseDestination == indexCase - 1)
                    {
                        // On definit la prise en passant a gauche
                        Deplacement priseEnPassant = new Deplacement(indexCase, indexCase - 9, Piece.PION_BLANC);
                        priseEnPassant.pieceCapturee = Piece.PION_NOIR;
                        priseEnPassant.typeDeplacement = TypeDeplacement.DEPLACEMENT_CAPTURE_EN_PASSANT;
                        // On ajoute le deplacement a la liste
                        cases.Add(priseEnPassant);
                    }
                    else if (dep.caseDestination == indexCase + 1)
                    {
                        // On definit la prise en passant a gauche
                        Deplacement priseEnPassant = new Deplacement(indexCase, indexCase - 7, Piece.PION_BLANC);
                        priseEnPassant.pieceCapturee = Piece.PION_NOIR;
                        priseEnPassant.typeDeplacement = TypeDeplacement.DEPLACEMENT_CAPTURE_EN_PASSANT;
                        // On ajoute le deplacement a la liste
                        cases.Add(priseEnPassant);
                    }
                }
            }

            return cases;
        }

        public static long CapturesPossiblesPionBlanc(Plateau p,Case indexCase)
        {
            long cases = 0;
            Case i = indexCase;
            Piece piece = p.trouverPiece(indexCase);

            i -= 9;
            if ((int)i % 7 != 0 && i >= Case.A8)
            {
                if ((p.bitboards[(int)Piece.PIECES_NOIRES] & Plateau.BitsCases[(int)i]) != 0)
                {
                    ajoutCasePossible(p, piece, i, ref cases);
                }
            }
            i += 2;
            if ((int)i % 8 != 0 && i>= Case.A8)
            {
                if ((p.bitboards[(int)Piece.PIECES_NOIRES] & Plateau.BitsCases[(int)i]) != 0)
                {
                    ajoutCasePossible(p, piece, i, ref cases);
                }
            }
            return cases;
        }

        public static List<Deplacement> CoupsPossiblesPionNoir(Plateau p, Case indexCase)
        {
            List<Deplacement> cases = new List<Deplacement>();
            Case i = indexCase;
            Piece piece = Piece.PION_NOIR;
            Deplacement dep = new Deplacement();

            // On regarde si on peut avancer tout droit
            if (i + 8 <= Case.H1)
            {
                bool caseLibre = ajoutDeplacementPion(p, piece, i + 8, indexCase, ref cases);
                // Si le pion est sur la premiere ligne, on teste une case supplementaire
                if ((i >= Case.A7) && (i <= Case.H7) && caseLibre)
                {
                    ajoutDeplacementPion(p, piece, i + 16, indexCase, ref cases);
                }
            }

            // On regarde si on peut prendre des pieces
            i = indexCase + 9;
            if ((int)i % 8 != 0 && i <= Case.H1 && ((p.bitboards[(int)Piece.PIECES_BLANCHES] & Plateau.BitsCases[(int)i]) != 0))
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }
            i = indexCase + 7;
            if ((int)i % 8 != 7 && i <= Case.H1 && ((p.bitboards[(int)Piece.PIECES_BLANCHES] & Plateau.BitsCases[(int)i]) != 0))
            {
                ajoutDeplacementPossible(p, piece, i, indexCase, ref cases);
            }

            // On regarde si une prise en passant est possible
            // Si le pion est sur la 5e ligne
            if (indexCase >= Case.A4 && indexCase <= Case.H4)
            {
                dep = p.dernierDeplacement;
                if( dep !=null)
                // Si le dernier deplacement est un pion adverse qui avance de deux cases
                if (dep.typeDeplacement == TypeDeplacement.DEPLACEMENT_NORMAL && dep.pieceDeplacee == Piece.PION_BLANC
                    && dep.caseSource >= Case.A2 && dep.caseSource <= Case.H2 && dep.caseDestination == dep.caseSource - 16)
                {
                    // Si le pion adverse atterit a gauche du pion courant
                    if (dep.caseDestination == indexCase - 1)
                    {
                        // On definit la prise en passant a gauche
                        Deplacement priseEnPassant = new Deplacement(indexCase, indexCase + 7, Piece.PION_NOIR);
                        priseEnPassant.pieceCapturee = Piece.PION_BLANC;
                        priseEnPassant.typeDeplacement = TypeDeplacement.DEPLACEMENT_CAPTURE_EN_PASSANT;
                        // On ajoute le deplacement a la liste
                        cases.Add(priseEnPassant);
                    }
                    else if (dep.caseDestination == indexCase + 1)
                    {
                        // On definit la prise en passant a gauche
                        Deplacement priseEnPassant = new Deplacement(indexCase, indexCase + 9, Piece.PION_NOIR);
                        priseEnPassant.pieceCapturee = Piece.PION_BLANC;
                        priseEnPassant.typeDeplacement = TypeDeplacement.DEPLACEMENT_CAPTURE_EN_PASSANT;
                        // On ajoute le deplacement a la liste
                        cases.Add(priseEnPassant);
                    }
                }
            }

            return cases;
        }
        
        public static long CapturesPossiblesPionNoir(Plateau p,Case indexCase)
        {
            long cases = 0;
            Case i = indexCase;
            Piece piece = p.trouverPiece(indexCase);

            i += 7;
            if ((int)i % 7 != 0 && i<=Case.H1)
            {
                if ((p.bitboards[(int)Piece.PIECES_BLANCHES] & Plateau.BitsCases[(int)i]) != 0)
                {
                    ajoutCasePossible(p, piece, i, ref cases);
                }
            }

            i += 2;
            if ((int)i % 8 != 0 && i <= Case.H1)
            {
                if ((p.bitboards[(int)Piece.PIECES_BLANCHES] & Plateau.BitsCases[(int)i]) != 0)
                {
                    ajoutCasePossible(p, piece, i, ref cases);
                }
            }
            return cases;
        }

        public void Copy(Deplacement target)
        {
            pieceDeplacee = target.pieceDeplacee;
            pieceCapturee = target.pieceCapturee;
            caseDestination = target.caseDestination;
            caseSource = target.caseSource;
            typeDeplacement = target.typeDeplacement;
            evaluationDeplacement = target.evaluationDeplacement;
            typeEvaluationDeplacement = target.typeEvaluationDeplacement;
            profondeurRecherche = target.profondeurRecherche;

        }

        public override string ToString()
        {
		return base.ToString() + ": CaseDepart " + caseSource.ToString() + " Case Arrivee " + caseDestination.ToString() + " Piece Mouvante : " + pieceDeplacee
                +" Piece Capturee " + pieceCapturee + " Evaluation : " + evaluationDeplacement + " TYPEDEPLACEMENT " + typeDeplacement + " Promotion : " + promotion;
        }

        public override Boolean Equals(object o)
        {
            if (o == null)
                return false;
            Deplacement d = (Deplacement)o;
            if (d.caseDestination == caseDestination && d.caseSource == caseSource
                && d.typeDeplacement == typeDeplacement && d.pieceDeplacee == pieceDeplacee
                && d.pieceCapturee == pieceCapturee)
                return true;
            else
                return false;

        }

    }

}
