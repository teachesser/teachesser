﻿using System;
using System.Collections.Generic;
using Emgu.CV.Structure;

namespace Teachesser.Model
{
    /// <summary>
    /// Classe permettant de faire le lien entre l'Enum case et les coordonées en XY
    /// </summary>
    class CaseXY
    {
        public int x, y;
        
        String colonne;
        String ligne;
        static int xBase = 330;
        static int yBase = 122;
        static int deltaXY = 30;
        public Case casePlateau;
        public static List<CaseXY> listeCases = new List<CaseXY>();
        public static List<CaseXY> listeCasesOccupees = new List<CaseXY>();
        static CaseXY()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    listeCases.Add(new CaseXY(j, i));
                }
            }
            
        }
        private CaseXY(int i,int j)
        {
            casePlateau = (Case)( 8 * j + i);
            this.x = xBase + i * App.HAUTEURCASE;
            this.y = yBase +j  * App.HAUTEURCASE;
            colonne = i.ToString();
            ligne = j.ToString();
        }
        public static List<Case> casesOccupees(CircleF[] tableauCercles)
        {
            List<Case> casesOccupees = new List<Case>();
            CaseXY caseXY;
            foreach (CircleF circle in tableauCercles)
            {
                caseXY = caseOccupee((int)circle.Center.X, (int)circle.Center.Y);
                if (caseXY != null)
                    casesOccupees.Add(caseXY.casePlateau);
            }
            return casesOccupees;
        }

        private static CaseXY caseOccupee(int x, int y)
        {
            foreach (CaseXY caseE in listeCases)
            {

                if (caseE.x + deltaXY > x && caseE.x - deltaXY < x)
                    if (caseE.y + deltaXY > y && caseE.y - deltaXY < y)
                        return caseE;
            }
            return null;
        }

        public static Case casePieceLevee(List<Case> casesOccupeesUI, List<Case> casesOccupeesPlateau)
        {

            foreach (Case casePlateau in casesOccupeesPlateau)
            {
                if (!casesOccupeesUI.Contains(casePlateau))
                    return casePlateau;
            }
            return Case.NULL_CASE;
        }


        public static List<Case> casesPiecesLevees(List<Case> casesOccupeesUI, List<Case> casesOccupeesPlateau)
        {
            List<Case> listeCases = new List<Case>();
            foreach (Case casePlateau in casesOccupeesPlateau)
            {
                if (!casesOccupeesUI.Contains(casePlateau))
                    listeCases.Add(casePlateau);
            }
            return listeCases;
        }

        public static Case casePiecePosee(List<Case> casesOccupeesUI, List<Case> casesOccupeesPlateau)
        {
            foreach (Case caseUI in casesOccupeesUI)
            {
                if (!casesOccupeesPlateau.Contains(caseUI))
                    return caseUI;
            }
            return Case.NULL_CASE;
        }


        public override String ToString()
        {
            return casePlateau.ToString();
        }


    }
}
