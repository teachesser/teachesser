﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;

namespace Teachesser.Resources
{
    /// <summary>
    /// Interaction logic for _3.xaml
    /// </summary>
    public partial class _3 : TagVisualization
    {
        public _3()
        {
            InitializeComponent();
        }

        private void _3_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO: customize _3's UI based on this.VisualizedTag here
        }
    }
}
