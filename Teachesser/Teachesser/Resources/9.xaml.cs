﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;

namespace Teachesser.Resources
{
    /// <summary>
    /// Interaction logic for _9.xaml
    /// </summary>
    public partial class _9 : TagVisualization
    {
        public _9()
        {
            InitializeComponent();
        }

        private void _9_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO: customize _9's UI based on this.VisualizedTag here
        }
    }
}
