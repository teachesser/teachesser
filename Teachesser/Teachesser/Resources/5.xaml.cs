﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;

namespace Teachesser.Resources
{
    /// <summary>
    /// Interaction logic for _5.xaml
    /// </summary>
    public partial class _5 : TagVisualization
    {
        public _5()
        {
            InitializeComponent();
        }

        private void _5_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO: customize _5's UI based on this.VisualizedTag here
        }
    }
}
