﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;

namespace Teachesser.Resources
{
    /// <summary>
    /// Interaction logic for _2.xaml
    /// </summary>
    public partial class _2 : TagVisualization
    {
        public _2()
        {
            InitializeComponent();
        }

        private void _2_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO: customize _2's UI based on this.VisualizedTag here
        }
    }
}
