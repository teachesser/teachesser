﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;

namespace Teachesser.Resources
{
    /// <summary>
    /// Interaction logic for _8.xaml
    /// </summary>
    public partial class _8 : TagVisualization
    {
        public _8()
        {
            InitializeComponent();
        }

        private void _8_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO: customize _8's UI based on this.VisualizedTag here
        }
    }
}
