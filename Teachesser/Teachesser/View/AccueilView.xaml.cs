﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Teachesser.ViewModel;

namespace Teachesser.View
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class AccueilView: UserControl
    {
        private AccueilViewModel avm = new AccueilViewModel();
        
        #region CONSTRUCTEURS
        /// <summary>
        /// Initialise le dataContext
        /// </summary>
        public AccueilView()
        {
            this.DataContext = avm;
            InitializeComponent();
            this.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
            this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
        }
        #endregion 

        #region Méthode des composants

        /// <summary>
        /// Allez sur le plateau de jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeToOptionsPartie(object sender, RoutedEventArgs e)
        {
            //Le code qu'il faut remettre
            FenetrePrincipale.Fvm.Content = new  View.OptionPartieView();
            // Le code pour faire des tests
           // FenetrePrincipale.Fvm.Content = new View.InterfaceJeu.InterfacePlateau(new Model.Partie.Partie());
        }

        /// <summary>
        /// Bouton exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Change le User Control par OptionsView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeToOptionsView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new View.OptionsView();
        }

        /// <summary>
        /// Change le User Control par ScoresView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeToScoresView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new View.ScoresView();
        }

        #endregion

       

    }
}
