﻿using System.Windows;
using System.Windows.Media;
using Teachesser.ViewModel;
using Microsoft.Surface.Presentation.Controls;

namespace Teachesser.View
{
    /// <summary>
    /// Unique fenetre de l'application 
    /// dont le contenu est un User Control
    /// Singleton
    /// </summary>
    public partial class FenetrePrincipale : SurfaceWindow
    {
        private static FenetreViewModel _fvm = new FenetreViewModel();
        private static FenetrePrincipale f   = new FenetrePrincipale(); 

        #region CONSTRUCTEURS
        /// <summary>
        /// Initialise le dataContext avec le ViewModel
        /// </summary>
        public FenetrePrincipale()
        {
            this.DataContext = _fvm;
            InitializeComponent();
            
        }

        public static FenetrePrincipale getInstance()
        {
            return f;
        }
        #endregion

        #region GETTERS ET SETTERS
        /// <summary>
        /// Signleton sur le viewModel de la Fenetre
        /// </summary>
        public static FenetreViewModel Fvm
        {
            get{return _fvm;}
        }

        #endregion 
    }
}
