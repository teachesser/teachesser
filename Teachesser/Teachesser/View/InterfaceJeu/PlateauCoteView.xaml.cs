﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Surface.Presentation.Controls;
using Teachesser.Model;


namespace Teachesser.View.InterfaceJeu
{
    /// <summary>
    /// Logique d'interaction pour PlateauDroitView.xaml
    /// </summary>
    public partial class PlateauCoteView : UserControl
    {
        private InterfacePlateau _parent;
        private JoueurConcret _jC;
        private String _nom;
        public Deplacement d = null;


        #region CONSTRUCTEUR
        public PlateauCoteView(InterfacePlateau parent, JoueurConcret jC)
        {
            this._jC = jC;
            this._parent = parent;
            InitializeComponent();
            _nom = _jC.PF.Pseudo;
        }
        #endregion

        #region Commande - Fin de tour
        private void finDeTour_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            _parent.Pvm.jouerMouvement();
            // Change le focus pour actualiser la methode CanExecute et ainsi griser le bouton
            this.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private void finDeTour_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_jC.Couleur == 0)
                e.CanExecute = (_parent.Pvm.Plateau.JoueurCourant == 0);
            else if (_jC.Couleur == 1)
                e.CanExecute = (_parent.Pvm.Plateau.JoueurCourant == 1);
        }
        #endregion

        #region Commande - Options
        private void options_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            if (_jC.Couleur == JoueurConcret.COTE_BLANC)
            {
                _parent.Pvm.PartieDroite = new PlateauMenuCoteView(_parent, _jC, this);
            }
            else
            {
                _parent.Pvm.PartieGauche = new PlateauMenuCoteView(_parent, _jC, this);
            }
        }

        private void options_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion

        #region Commande - Affichage de l'aide
        private void deplacementIA_Executed(Object sender, ExecutedRoutedEventArgs e)
        {

            progressCalculIA.Value = 0;

            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Deplacement d = _parent.abc.choisirMeilleurDeplacement(_parent.Pvm.Plateau, sender as BackgroundWorker);
            e.Result      =  d;
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressCalculIA.Value = e.ProgressPercentage;
            aide.IsEnabled = false;                 
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            d = (Deplacement)e.Result;
            TagVisualizer source = CaseDefinition.getVisu(d.caseSource);
            TagVisualizer dest   = CaseDefinition.getVisu(d.caseDestination);
            progressCalculIA.Value = 100;

            changeStyleTagVisualizer(source, 1, 5, System.Windows.Media.Brushes.Green);
            changeStyleTagVisualizer(dest, 1, 5, System.Windows.Media.Brushes.Gold);
            aide.IsEnabled = true;

            aideIALabel.Content = d.caseSource + " → " + d.caseDestination;
        }

        private void deplacementIA_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_jC.Couleur == 0)
                e.CanExecute = (_parent.Pvm.Plateau.JoueurCourant == 0);

            else if(_jC.Couleur == 1)
                e.CanExecute = (_parent.Pvm.Plateau.JoueurCourant == 1);

        }
        #endregion

        public void changeStyleTagVisualizer(TagVisualizer t, double opacite, int thickness, SolidColorBrush sCD)
        {
            t.Opacity = opacite;
            t.BorderThickness = new Thickness(thickness);
            t.BorderBrush = sCD;
        }

        public String Nom
        {
            get { return _nom; }
        }

    }
}

