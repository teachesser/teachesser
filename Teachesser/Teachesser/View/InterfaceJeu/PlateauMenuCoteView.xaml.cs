﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Teachesser.Model;

namespace Teachesser.View.InterfaceJeu
{
    /// <summary>
    /// Logique d'interaction pour PlateauMenuGaucheView.xaml
    /// </summary>
    public partial class PlateauMenuCoteView : UserControl
    {
        private InterfacePlateau _parent;
        private JoueurConcret    _jC;
        private UserControl      plateauCoteView;

        public PlateauMenuCoteView(InterfacePlateau parent, JoueurConcret jC, UserControl plateauCoteView)
        {
            this._jC = jC;
            this._parent = parent;
            this.plateauCoteView = plateauCoteView;
            InitializeComponent();
        }

        #region Interaction
        private void Capitule_Click(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new AccueilView();  
        }

        private void Annuler_Click(object sender, RoutedEventArgs e)
        {
            if (_jC.Couleur == JoueurConcret.COTE_BLANC)
            {
                _parent.Pvm.PartieDroite = plateauCoteView;
            }
            else
            {
                _parent.Pvm.PartieGauche = plateauCoteView;
            }
        }
        #endregion
    }
}
