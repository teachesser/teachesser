﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Surface.Presentation.Controls;
using Teachesser.Model;
using Teachesser.Model.Partie;
using Teachesser.Model.IntelligenceArtificielle;
using Teachesser.ViewModel;
using Microsoft.Surface.Presentation.Input;
using System.ComponentModel;
using Microsoft.Surface.Core;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Surface;
using Microsoft.Win32;
using System.Text;
using Teachesser.Util;
using System.Drawing;
using System.Diagnostics;
using Emgu.CV.Structure;
using Emgu.CV;
using Emgu.CV.CvEnum;
using System.Timers;
using System.Windows.Threading;

namespace Teachesser.View.InterfaceJeu
{
    /// <summary>
    /// Logique d'interaction pour InterfacePlateau.xaml
    /// composé d'un singleton 
    /// </summary>
    /// <author>Raphaël Turquet, Alban Rousseau</author>
    public partial class InterfacePlateau : UserControl
    {
        //Architecture
        public SurfaceWindow f = FenetrePrincipale.getInstance();
        public PlateauViewModel _pvm;

        //Tactile
        private TouchTarget touchTarget;        //Attributs pour la prise d'image
        private IntPtr hwnd;
        private bool dansInterfaceJeu = false;

        //Jeu
        public int nbPiecePlateau = 0;
        CaseDefinition cd;                      // case plus son tagVisualiser associé

        //IA
        public AlphaBetaChercheur abc;         // Permet de trouver le meilleur coup possible for now

        // Compte à rebours
        //Timer compteARebours;
        public DispatcherTimer compteARebours1;
        public DispatcherTimer compteARebours2;
        int heure1, minute1, seconde1;
        int heure2, minute2, seconde2;

        #region CONSTRUCTEURS
        public InterfacePlateau(Partie partie)
        {
            _pvm = new PlateauViewModel();
            Pvm.InterfacePlateau = this;

            //Initialisation du plateau
            this.DataContext = Pvm;
            InitializeComponent();

            //Paramètre plateau
            Grid g = (Grid)this.FindName("nomGrid");
            g.Height = 840;
            g.Width = g.Height;
            f = FenetrePrincipale.getInstance();

            // Création de la partie
            this._pvm.Partie = partie;
            this.cd = new CaseDefinition(this);
            this.abc = new AlphaBetaChercheur();
            this.nbPiecePlateau   = _pvm.Plateau.plateauDepart();
            //this.nbPiecePlateau = _pvm.Plateau.plateauTest();
            dansInterfaceJeu = true;
            this._pvm.PartieDroite = new PlateauCoteView(this, _pvm.Partie.JoueurBlanc);
            this._pvm.PartieGauche = new PlateauCoteView(this, _pvm.Partie.JoueurNoir);

            // Initialisation du compteur
            //if(_pvm.isPlateauPret)
                this.initCompteARebours();

            InitializeSurfaceInput();

        }
        #endregion

        #region Surface, tactile, ...
        /// <summary>
        /// Initialisation du tactile
        /// </summary>
        private void InitializeSurfaceInput()
        {
            // Get the hWnd for the SurfaceWindow object after it has been loaded.
            hwnd = new System.Windows.Interop.WindowInteropHelper(f).Handle;
            touchTarget = new Microsoft.Surface.Core.TouchTarget(hwnd);

            // Set up the TouchTarget object for the entire SurfaceWindow object.
            touchTarget.EnableInput();
            touchTarget.EnableImage(ImageType.Normalized);

            // Attach an event handler for the FrameReceived event.
            touchTarget.FrameReceived += new EventHandler<FrameReceivedEventArgs>(OnTouchTargetFrameReceived);
        }


        private void OnTouchTargetFrameReceived(object sender, Microsoft.Surface.Core.FrameReceivedEventArgs e)
        {
            if (!dansInterfaceJeu)
                Console.WriteLine("useful?");

            _pvm.algoDuJeu(e);
        }

        /// <summary>
        /// Changer le style d'un tag
        /// </summary>
        public void changeStyleTagVisualizer(TagVisualizer t, double opacite, int thickness, SolidColorBrush sCD)
        {
            t.Opacity = opacite;
            t.BorderThickness = new Thickness(thickness);
            t.BorderBrush = sCD;
        }
        #endregion

        #region Compteur de temps
        /// <summary>
        /// Initialisation du compte à rebours
        /// </summary>
        public void initCompteARebours()
        {
            // Conversion en HH:MM:SS
            this.heure1 = (int)this._pvm.Partie.TempsDeJeu / 60;
            this.minute1 = this._pvm.Partie.TempsDeJeu - 60 * this.heure1;
            this.seconde1 = 0;
            this.heure2 = (int)this._pvm.Partie.TempsDeJeu / 60;
            this.minute2 = this._pvm.Partie.TempsDeJeu - 60 * this.heure2;
            this.seconde2 = 0;

            // Initialisation du temps
            this.CompteurTemps1.Content = this._pvm.Partie.TempsDeJeu;
            this.CompteurTemps2.Content = this._pvm.Partie.TempsDeJeu;

            // Création des comptes à rebours
            this.compteARebours1 = new DispatcherTimer();
            this.compteARebours2 = new DispatcherTimer();
            compteARebours1.Interval = TimeSpan.FromSeconds(1);
            compteARebours1.Tick += minuterie;
            compteARebours2.Interval = TimeSpan.FromSeconds(1);
            compteARebours2.Tick += minuterie;

            // Lancement du compte à rebours du jour 1
            //if(_pvm.isPlateauPret)
                compteARebours1.Start();
        }

        /// <summary>
        /// Fonction qui se déclenche toutes les secondes du compte à rebours
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void minuterie(object sender, EventArgs e)
        {
            // Si la partie se termine
            if (testFinPartie())
            {
                compteARebours1.Stop();
                compteARebours2.Stop();
                this.CompteurTemps1.Content = "fini";
                FenetrePrincipale.Fvm.Content = new FinPartieView(_pvm);
            }
            // Tant que la partie n'est pas finie
            else
            {
                // Gestion du temps du joueur 1
                if (_pvm.Plateau.JoueurCourant == 0)
                {
                    compteARebours2.Stop();

                    // Si le compteur arrive à 0
                    if (this.seconde1 == 0 && this.minute1 == 0 && this.heure1 == 0)
                    {
                        _pvm.Plateau.tempsFini = true;
                    }
                    else
                    {
                        // Décrémentation des secondes
                        this.seconde1--;

                        // Décrémentation des minutes
                        if (this.seconde1 == -1)
                        {
                            this.seconde1 = 59;
                            this.minute1--;
                        }

                        // Décrémentation des heures
                        if (this.minute1 == -1)
                        {
                            this.minute1 = 59;
                            this.heure1--;
                        }

                        // MAJ du label du compteur
                        this.CompteurTemps1.Content = parseTemps(this.heure1, this.minute1, this.seconde1);
                    }
                }
                // Gestion du temps du joueur 2
                else
                {
                    compteARebours1.Stop();

                    // Si le compteur arrive à 0
                    if (this.seconde2 == 0 && this.minute2 == 0 && this.heure2 == 0)
                    {
                        _pvm.Plateau.tempsFini = true;
                    }
                    else
                    {
                        // Décrémentation des secondes
                        this.seconde2--;

                        // Décrémentation des minutes
                        if (this.seconde2 == -1)
                        {
                            this.seconde2 = 59;
                            this.minute2--;
                        }

                        // Décrémentation des heures
                        if (this.minute2 == -1)
                        {
                            this.minute2 = 59;
                            this.heure2--;
                        }

                        // MAJ du label du compteur
                        this.CompteurTemps2.Content = parseTemps(this.heure2, this.minute2, this.seconde2);
                    }
                }
            }
        }

        /// <summary>
        /// Parse le temps en string pour un bel affichage
        /// </summary>
        /// <param name="heure"></param>
        /// <param name="minute"></param>
        /// <param name="seconde"></param>
        /// <returns></returns>
        public String parseTemps(int heure, int minute, int seconde)
        {
            // Affichage avec les 0
            string.Format("{0:00}", heure);
            string.Format("{0:00}", minute);
            string.Format("{0:00}", seconde);

            String temps = heure.ToString("00") + ":" + minute.ToString("00") + ":" + seconde.ToString("00");

            return temps;
        }
        #endregion

        /// <summary>
        /// Test la fin de la partie
        /// </summary>
        /// <returns>true si la partie est finie</returns>
        public bool testFinPartie()
        {
            if (_pvm.Plateau.echecMat == true || _pvm.Plateau.pat == true || _pvm.Plateau.tempsFini == true)
            {
                return true;
            }

            return false;
        }

        #region GETTERS ET SETTERS

        public PlateauViewModel Pvm
        {
            get { return _pvm; }
        }

        #endregion
    }
}