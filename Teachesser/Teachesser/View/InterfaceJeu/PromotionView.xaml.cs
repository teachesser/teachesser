﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using Teachesser.ViewModel;
using Teachesser.Model;

namespace Teachesser.View.InterfaceJeu
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class PromotionView : UserControl
    {
        private PlateauViewModel pvm;
        private UserControl coteView;
        private Deplacement deplacement;

        public PromotionView(PlateauViewModel pvm, UserControl coteView, Deplacement deplacement)
        {
            this.pvm = pvm;
            this.deplacement = deplacement;
            this.coteView = coteView;
            InitializeComponent();
        }

        /// <summary>
        /// Effectue la promotion d'un pion
        /// </summary>
        /// <param name="sender">Bouton avec pour attribut Name le nom de la piece choisie</param>
        /// <param name="e"></param>
        private void PromotePawn(object sender, RoutedEventArgs e)
        {
            String nomPiece = ((SurfaceButton)sender).Name;

            switch (nomPiece)
            {
                case "Dame" :
                    doPromotion(Piece.DAME_NOIRE - pvm.Plateau.JoueurCourant);
                    break;

                case "Fou" :
                    doPromotion(Piece.FOU_NOIR - pvm.Plateau.JoueurCourant);
                    break;

                case "Tour" :
                    doPromotion(Piece.TOUR_NOIRE - pvm.Plateau.JoueurCourant);
                    break;

                case "Cavalier" :
                    doPromotion(Piece.CAVALIER_NOIR - pvm.Plateau.JoueurCourant);
                    break;
            }
        }

        private void doPromotion(Piece piecePromotion)
        {
            // Effectue la promotion du pion dans le modele
            pvm.Plateau.promouvoirPion(deplacement.caseDestination, deplacement.pieceDeplacee, piecePromotion);

            // Indique au PlateauViewModel de remettre le PlateauCoteView
            if (pvm.Plateau.JoueurCourant == Joueur.COTE_BLANC)
            {
                pvm.PartieDroite = coteView;
            }
            else
            {
                pvm.PartieGauche = coteView;
            }
        }
    }
}
