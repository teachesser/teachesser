﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Teachesser.Model;
using Teachesser.ViewModel;

/// <summary>
/// Logique d'interaction pour FinPartieView.xaml
/// </summary>
/// <author>Raphaël Turquet</author>
namespace Teachesser.View.InterfaceJeu
{
    /// <summary>
    /// Logique d'interaction pour FinPartieView.xaml
    /// </summary>
    public partial class FinPartieView : UserControl
    {
        private PlateauViewModel pvm;
        private String J1;
        private String J2;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="plateau"></param>
        public FinPartieView(PlateauViewModel pvm)
        {
            // Récupération de la partie
            this.pvm = pvm;

            // Récupéaration des noms des joueurs
            if (this.pvm.Plateau.JoueurCourant == 0)
            {
                this.J1 = this.pvm.Partie.JoueurBlanc.PF.Pseudo;
                this.J2 = this.pvm.Partie.JoueurNoir.PF.Pseudo;
            }
            else
            {
                this.J1 = this.pvm.Partie.JoueurNoir.PF.Pseudo;
                this.J2 = this.pvm.Partie.JoueurBlanc.PF.Pseudo;
            }

            InitializeComponent();

            // Initialisation du message de fin
            this.messageFinPartie.Content = messageFin();
        }

        public FinPartieView()
        {
            // TODO: Complete member initialization
        }

        #region Gestion de fin de partie
        /// <summary>
        /// Initialisation du message de fin
        /// </summary>
        public String messageFin()
        {
            String messageFin = "";

            // Cas de l'échec et mat
            if (this.pvm.Plateau.echecMat)
            {
                messageFin = this.J2 + " gagne la partie contre " + this.J1 + " par échec et mat !";
            }
            else
            {
                // Cas d'un pat
                if (this.pvm.Plateau.pat)
                {
                    messageFin = "La partie entre " + this.J2 + " et " + this.J1 + " se termine par un pat.";
                }
                // Cas où le temps d'un des joueurs se termine
                else
                {
                    messageFin = "Le temps de jeu de " + this.J1 + " est terminé ! " + this.J2 + " remporte la partie.";
                }
            }

            return messageFin;
        }
        #endregion

        /// <summary>
        /// Aller à l'accueil
        /// <summary>    
        private void ChangeToAccueilView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new AccueilView();
        }
    }
}
