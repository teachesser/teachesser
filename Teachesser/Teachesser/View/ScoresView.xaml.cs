﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Teachesser.Util;
using Teachesser.Model;
using System.Configuration;
using System.Collections.ObjectModel;
using Teachesser.ViewModel;

/// <summary>
/// Logique d'interaction pour OptionsView.xaml
/// Singleton
/// </summary>
/// <author> Raphaël Turquet </author>
namespace Teachesser.View
{
    public partial class ScoresView : UserControl
    {
        private static ScoresView sW = new ScoresView();
        private ScoresViewModel sWM  = new ScoresViewModel();

        #region Constructeur
        public ScoresView()
        {
            // Initialisation du tableau des scores
            this.DataContext = sWM;
            InitializeComponent();
        }
        #endregion

        /// <summary>
        /// Retour à l'accueil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void ChangeToAccueilView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new AccueilView();
        }
    }
}
