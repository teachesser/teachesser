﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Teachesser.ViewModel;

namespace Teachesser.View
{
    /// <summary>
    /// Logique d'interaction pour OptionsView.xaml
    /// </summary>
    public partial class OptionsView : UserControl
    {
        private static OptionsView oW = new OptionsView();
        public OptionsViewModel ovm = new OptionsViewModel();

        #region CONSTRUCTEURS
        public OptionsView()
        {
            InitializeComponent();
        }
        #endregion
        
        public static UserControl getInstance(){ return oW;}

        /// <summary>
        /// Réinitialisation de la BDD des scores
        /// </summary>
        private void ReinitialisationScore(object sender, RoutedEventArgs e)
        {
            this.ovm.reinitialisationScore();
            RetourLabel.Content = "Réinitialisation effectuée.";
        }

        /// <summary>
        /// Retour à l'accueil
        /// </summary>
        private void ChangeToAccueilView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new AccueilView();
        }

    }
}
