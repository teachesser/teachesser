﻿using System;
using System.Windows;
using System.Windows.Controls;
using Teachesser.Model;
using Teachesser.Model.BDDScores;

namespace Teachesser.View.Creation
{
    /// <summary>
    /// Logique d'interaction pour la création du profil pour le seond joueur
    /// </summary>
    public partial class CreationJoueur2View : UserControl
    {
        private OptionPartieView _parent;

        public CreationJoueur2View(OptionPartieView parent)
        {
            this._parent = parent;
            InitializeComponent();
        }

        #region CREATION DU JOUEUR
        /// <summary>
        /// Création du joueur
        /// </summary>
        private void CreationDuJoueur(object sender, RoutedEventArgs e)
        {
            String textInput = PseudoJoueur.Text;
            ProfilJoueur pf  = new ProfilJoueur(textInput);
            bool succes      = false;

            //Vérication textBox non vide
            if (textInput.CompareTo("") == 1)
            {
                // Si l'insertion est réussite
                if (succes = pf.InsererJoueur())
                {
                    _parent.optvm.Data = GestionnaireBDD.ReadXML();
                    RetourLabel.Content = "Votre compte a été créé !";
                    FenetrePrincipale.Fvm.Content = new View.OptionPartieView();
                }
            }
            else
            {
                RetourLabel.Content = "Le pseudo est incorrect ou existe déjà !";
            }    
        }
        #endregion

        /// <summary>
        /// Retour aux options de la partie
        /// </summary>
        private void ChangeToOptionPartieView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new View.OptionPartieView();
        }
    }
}
