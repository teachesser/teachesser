﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Teachesser.ViewModel;
using Teachesser.View.Creation;

namespace Teachesser.View
{
    /// <summary>
    /// Logique d'interaction pour OptionPartieView.xaml
    /// </summary>
    /// <author> Raphaël Turquet </author>
    public partial class OptionPartieView : UserControl
    {
        public OptionPartieViewModel optvm  = new OptionPartieViewModel();

        #region CONSTRUCTEURS
        /// <summary>
        /// Constructeur
        /// </summary>
        public OptionPartieView()
        {
            optvm.PartieJoueur2 = new OptionJoueur2View(this);
            optvm.PartieJoueur1 = new OptionJoueurView(this);
            this.DataContext = optvm;
            InitializeComponent();
        }
        #endregion

        #region GETTERS ET SETTERS
        #endregion
    }
}