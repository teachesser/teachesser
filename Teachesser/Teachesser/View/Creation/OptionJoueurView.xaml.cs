﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Teachesser.Model.Partie;

namespace Teachesser.View.Creation
{
    /// <summary>
    /// Logique d'interaction pour OptionJoueurPrincipalView.xaml
    /// </summary>
    public partial class OptionJoueurView : UserControl
    {
        private OptionPartieView    parent;

        public OptionJoueurView(OptionPartieView parent)
        {
            this.parent     = parent;
            InitializeComponent();
        }

        #region Vérification joueur Pret
        /// <summary>
        /// Gestion bouton prêt du J1
        /// </summary>
        private void TestJoueur1Pret(object sender, RoutedEventArgs e)
        {

                String textProfil;
                String[] modeJeu;
                String textModeDeJeu;
                String textTdp;

                if (comboBoxProfil.SelectedValue != null && comboBoxModeDeJeu.SelectedItem != null && comboBoxTempsDeJeu != null)
                {
                    textProfil       = comboBoxProfil.SelectedValue.ToString();
                    modeJeu          = comboBoxModeDeJeu.SelectedItem.ToString().Split(new char[] { ' ' });
                    textModeDeJeu    = modeJeu[1];
                    textTdp          = comboBoxTempsDeJeu.Text;

                    foreach (String element in modeJeu)
                    {
                        System.Console.WriteLine(element);
                    }

                    Console.WriteLine(textProfil);
                    Console.WriteLine(textTdp);
                    Console.WriteLine(textModeDeJeu);

                    // S'il était prêt précédemment
                    if (parent.optvm.PretJoueur1)
                    {
                        parent.optvm.PretJoueur1 = false;
                        btnPret1.Background = App.ROUGE_BRUSH;
                    }
                    else
                    {
                        //Vérifcation des informations rentrées.
                        if (textProfil.CompareTo("") == 1
                            && textModeDeJeu.CompareTo("") == 1
                            && textTdp.CompareTo("") == 1)
                        {
                            parent.optvm.Partie.JoueurBlanc.PF.Pseudo = textProfil;
                            parent.optvm.Partie.Mode = Int32.Parse("1");
                            parent.optvm.Partie.TempsDeJeu = Int32.Parse(textTdp);
                            parent.optvm.PretJoueur1 = true;
                            btnPret1.Background = App.VERT_BRUSH;
                        }
                    }
                }
                else
                {
                    RetourLabel.Content = "Un ou plusieurs champs non rempli !";
                }
        }

        /// <summary>
        /// Si le joueur effectue une modification alors qu'il est censé être prêt
        /// Supprime également le message d'erreur d'un champ non rempli
        /// </summary>
        private void AnulationPret(object sender, EventArgs e)
        {
            RetourLabel.Content = "";
            if (parent.optvm.PretJoueur1)
            {
                parent.optvm.PretJoueur1 = false;
                btnPret1.Background = App.ROUGE_BRUSH;
            }
        }
        #endregion

        #region Incrémentation / Décrémentation temps de jeu
        /// <summary>
        /// Décrémentation temps de jeu
        /// </summary>
        private void decremente(object sender, RoutedEventArgs e)
        {
            int temps = Int32.Parse(comboBoxTempsDeJeu.Text);

            if (temps > 1)
            {
                temps -= 1;
                comboBoxTempsDeJeu.Text = temps.ToString();
            }
        }
        /// <summary>
        /// Incrémentation temps de jeu
        /// </summary>
        private void incremente(object sender, RoutedEventArgs e)
        {
            int temps = Int32.Parse(comboBoxTempsDeJeu.Text);

            if (temps < 1000)
            {
                temps += 1;
                comboBoxTempsDeJeu.Text = temps.ToString();
            }
        }
        #endregion

        /// <summary>
        /// Allez à la création du joueur
        /// </summary>
        private void ChangeToCreationJoueur(object sender, RoutedEventArgs e)
        {
            parent.optvm.PartieJoueur1 = new CreationJoueurView(parent, this);
        }

        /// <summary>
        /// Aller à l'accueil
        /// <summary>    
        private void ChangeToAccueilView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new AccueilView();
        }
    }
}
