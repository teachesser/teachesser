﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Teachesser.ViewModel;
using Teachesser.Model;
using Teachesser.Model.BDDScores;
using System.Windows.Controls.Primitives;
using Teachesser.View.Creation;

namespace Teachesser.View.Creation
{
    /// <summary>
    /// Logique d'interaction pour CreationJoueurView.xaml
    /// </summary>
    /// <author> Raphaël Turquet </author>
    public partial class CreationJoueurView : UserControl
    {
        private OptionPartieView _parent;
        private OptionJoueurView _previous;
        
        #region CONSTRUCTEURS
        /// <summary>
        /// Initialise le dataContext
        /// </summary>
        public CreationJoueurView(OptionPartieView parent, OptionJoueurView previous)
        {
            this._previous = previous;
            this._parent   = parent;
            InitializeComponent();
        }
        #endregion 

        #region CREATION DU JOUEUR
        /// <summary>
        /// Création du joueur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreationDuJoueur(object sender, RoutedEventArgs e)
        {
            String textInput = PseudoJoueur.Text;
            ProfilJoueur pf  = new ProfilJoueur(textInput);
            bool succes      = false;

            //Vérication textBox non vide
            if (textInput.CompareTo("") == 1)
            {
                
                // Si l'insertion est réussite
                if (succes = pf.InsererJoueur())
                {
                    _parent.optvm.Data = GestionnaireBDD.ReadXML();
                    RetourLabel.Content = "Votre compte a été créé !";
                    _parent.optvm.PartieJoueur1 = _previous;
                }
            }
            else
            {
                RetourLabel.Content = "Le pseudo est incorrect ou existe déjà !";
            }    
        }
        #endregion


        /// <summary>
        /// Retour aux options de la partie
        /// </summary>
        private void ChangeToOptionPartieView(object sender, RoutedEventArgs e)
        {
            _parent.optvm.PartieJoueur1 = _previous;
        }
    }
}
