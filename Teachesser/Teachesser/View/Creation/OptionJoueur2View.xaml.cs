﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Teachesser.View.Creation
{
    /// <summary>
    /// Logigue d'interaction pour la création de la partie du point du vue du joueur 2
    /// </summary>
    /// <author> Raphaël Turquet </author>
    public partial class OptionJoueur2View : UserControl
    {
        private OptionPartieView parent;

        public OptionJoueur2View(OptionPartieView parent)
        {
            this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
            this.parent = parent;
            InitializeComponent();
        }

        #region Test si le joueur est prêt
        /// <summary>
        /// Gestion bouton prêt du J2
        /// </summary>
        private void TestJoueur2Pret(object sender, RoutedEventArgs e)
        {

                String textProfil;

                if (comboBoxProfil2.SelectedItem != null)
                {
                    textProfil = textProfil = comboBoxProfil2.SelectedValue.ToString();

                    // S'il était prêt précédemment
                    if (parent.optvm.PretJoueur2)
                    {
                        parent.optvm.PretJoueur2 = false;
                        BoutonPret.Background = App.ROUGE_BRUSH;
                    }
                    else
                    {
                        parent.optvm.Partie.JoueurNoir.PF.Pseudo = textProfil;
                        parent.optvm.PretJoueur2 = true;
                        BoutonPret.Background = App.VERT_BRUSH;
                    }
                }
                else
                {
                    RetourLabel.Content = "Un ou plusieurs champs non rempli !";
                }   
        }

        /// <summary>
        /// Si le joueur effectue une modification alors qu'il est censé être prêt
        /// Supprime également le message d'erreur d'un champ non rempli
        /// </summary>
        private void AnulationPret(object sender, EventArgs e)
        {
            RetourLabel.Content = "";
            if (parent.optvm.PretJoueur2)
            {
                parent.optvm.PretJoueur2 = false;
                BoutonPret.Background = App.ROUGE_BRUSH;
            }
        }
        #endregion 

        /// <summary>
        /// Allez à la création du joueur
        /// </summary>
        private void ChangeToCreationJoueur2(object sender, RoutedEventArgs e)
        {
            parent.optvm.PartieJoueur2    = new CreationJoueur2View(parent);
        }

        /// <summary>
        /// Retour à l'accueil
        /// </summary>
        private void ChangeToAccueilView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = new AccueilView();
        }
    }
}
