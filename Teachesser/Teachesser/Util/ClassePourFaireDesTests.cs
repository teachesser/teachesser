﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Teachesser.Model;
using Emgu.CV.Structure;
using System.Drawing;

namespace Teachesser.Util
{
    class ClassePourFaireDesTests
    {
        static void Main()
        {
            Plateau plateau = new Plateau();
            plateau.plateauDepart();
            CircleF[] circles = new CircleF[2];
            circles[0] = new CircleF(new PointF(330, 124), 20);
            circles[1] = new CircleF(new PointF(370, 124), 20);
            List<Case> casesUI = CaseXY.casesOccupees(circles);
            List<Case> casesPlateau = plateau.casesOccupees();
            //Affiche les déplacements possibles pour une case
            Case caseVide = CaseXY.casePieceLevee(casesUI, casesPlateau);
            if (caseVide != Case.NULL_CASE)
            {
                //surlignageCasesPossibles(plateau.coupsPossibles(caseVide));
                Console.WriteLine(caseVide);
            }
            Console.ReadLine();
        }
    }
}
