﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Teachesser.ViewModel;

namespace Teachesser.ViewModel
{
    /// <summary>
    /// ViewModel associé à Fenetre  Principale
    /// </summary>
    public partial class FenetreViewModel : ViewModelBase
    {
        //Contenu visuel de la fenetre
        private UserControl _content = new View.AccueilView();

        #region GETTERS ET SETTERS
        public UserControl Content
        {
            get {return _content;}
            set {
                    _content = value;
                    NotifyPropertyChanged("Content");
                }
        }
        #endregion
    }
}
