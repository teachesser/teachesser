﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Teachesser.ViewModel
{
    /// <summary>
    /// Classe Mère qui permet d'implémenter 
    /// de manière générale l'interface INotifyPropertyChanged
    /// </summary>
    public partial class ViewModelBase: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
