﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Teachesser.Model;
using Teachesser.Model.BDDScores;
using Teachesser.Model.Partie;
using Teachesser.View;
using System.Windows.Controls;
using Teachesser.View.Creation;
using Teachesser.View.InterfaceJeu;
using System.Windows;

/// <summary>
/// View Model option partie
/// </summary>
/// <author> Raphaël Turquet </author>
namespace Teachesser.ViewModel
{
    public class OptionPartieViewModel : ViewModelBase
    {
        //Interface
        private UserControl _partieJoueur1;
        private UserControl _partieJoueur2;

        //Données
        private ObservableCollection <ProfilJoueur> _data;
        private Partie _partie = new Partie();

        #region CONSTRUCTEURS
        public OptionPartieViewModel()
        {
            Data = GestionnaireBDD.ReadXML();
        }
        #endregion

        /// <summary>
        /// Si les deux joueurs sont prêts, passage au plateau de jeu
        /// </summary>
        private void commencerLaPartie()
        {
            if (Joueur1.Pret && Joueur2.Pret && Joueur1.PF.Pseudo != Joueur2.PF.Pseudo)
            {
                FenetrePrincipale.Fvm.Content = new InterfacePlateau(_partie);
            }
            else
            {
                // Si les joueurs ont le même pseudo, ils ne sont plus prêt
                if (Joueur1.Pret && Joueur2.Pret && Joueur1.PF.Pseudo == Joueur2.PF.Pseudo)
                {
                    MessageBox.Show("Les deux joueurs ne peuvent pas avoir le même pseudo !");
                }
            }
        }

        //Modification restante
        #region GETTERS ET SETTERS
        public ObservableCollection<ProfilJoueur> Data
        {
            get{return _data;}
            set
            {
                _data = value;
                NotifyPropertyChanged("Data");
            }
        }

        public Partie Partie
        {
            get { return _partie; }
            set
            {
                _partie = value;
                NotifyPropertyChanged("Partie");
            }
        }

        public JoueurConcret Joueur1
        {
            get { return Partie.JoueurBlanc; }
            set 
            {
                Partie.JoueurBlanc = value;
                NotifyPropertyChanged("Joueur1");
            }
        }

        public JoueurConcret Joueur2
        {
            get { return Partie.JoueurNoir; }
            set
            {
                Partie.JoueurBlanc = value;
                NotifyPropertyChanged("Joueur2");
            }
        }

        public bool PretJoueur1
        {
            get { return Joueur1.Pret; }
            set
            {
                Joueur1.Pret = value;
                commencerLaPartie();
            } 
        }

        public bool PretJoueur2
        {
            get{ return Joueur2.Pret; }
            set
            {
                Joueur2.Pret = value;
                commencerLaPartie();
            } 
        }

        public UserControl PartieJoueur1
        {
            get { return _partieJoueur1; }
            set
            {
                _partieJoueur1 = value;
                NotifyPropertyChanged("PartieJoueur1");
            }
        }

        public UserControl PartieJoueur2
        {
            get { return _partieJoueur2; }
            set
            {
                _partieJoueur2 = value;
                NotifyPropertyChanged("PartieJoueur2");
            }
        }

        #endregion
    }
}
