﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Teachesser.Model.BDDScores;

namespace Teachesser.ViewModel
{
    public class OptionsViewModel
    {
        /// <summary>
        /// Réinitialisation de la BDD des scores
        /// </summary>
        public void reinitialisationScore()
        {
            GestionnaireBDD bdd = new GestionnaireBDD();
            bdd.ReinitialisationScore();
        }
    }
}
