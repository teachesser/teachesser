﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Teachesser.Model;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media;
using Teachesser.Model.Partie;
using System.Windows.Controls;
using Teachesser.View;
using Microsoft.Surface.Core;
using System.Runtime.InteropServices;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing.Imaging;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using Teachesser.View.InterfaceJeu;


namespace Teachesser.ViewModel
{
    /// <summary>
    /// Partie MVVM du Plateau
    /// </summary>
    public class PlateauViewModel : ViewModelBase
    {
        private UserControl _partieGauche;
        private UserControl _partieDroite;
        private UserControl _interfacePlateau;
        private Partie _partie;
        private Plateau _plateau;
        private long oldTimeStamp;
        //Variables utiles pour la capture d'image
        private Boolean imageDisponible;
        private byte[] normalizedImage;
        private ImageMetrics imageMetrics;
        int paddingLeft, paddingRight;
        public Case casePieceLeveeCourante;
        public Case casePosee;
        List<Case> casesUI;
        List<Case> casesPlateau;
        ColorPalette pal;

        Case derniereCaseLevee;
        private bool isPossibleCapture = false;
        public bool isPlateauPret = false;

        private int nombrePiecesPlateau = 32;

        Deplacement deplacementCapture;

        public PlateauViewModel()
        {
            _plateau = new Plateau();

            oldTimeStamp = 0;
            imageDisponible = false;
        }

        #region METHODES
        public void algoDuJeu(Microsoft.Surface.Core.FrameReceivedEventArgs e)
        {
            long now = DateTime.Now.Ticks;
            // Permet de vérifier le temps qui s'est passé depuis le dernier appel
            long tickDelta = Math.Abs(now - oldTimeStamp);
            captureImage(e);
            // Si le temps entre deux images est inférieur à 100ms ou qu'une nouvelle image n'est pas disponible ==> stop
            if (tickDelta < 200 * 10000 || !imageDisponible)
                return;
            lock (this)
            {
                CircleF[] circles = traitementImage();
                casesUI = CaseXY.casesOccupees(circles);

                if (casesUI.Count() == nombrePiecesPlateau)
                    isPlateauPret = true;
                
                if (isPlateauPret)
                {
                    casesPlateau = _plateau.casesOccupees();
                    for (Case i = Case.A8; i < Case.H1; i++)
                    {
                        afficherImages(i);
                    }

                    //Case pièce levée
                    casePieceLeveeCourante = CaseXY.casePieceLevee(casesUI, casesPlateau);
                    

                    //on check si cette case est la même qu'au cycle précédent
                    if ((int)casePieceLeveeCourante == (int)derniereCaseLevee && casePieceLeveeCourante != Case.NULL_CASE)
                    {
                        //Si c'est pas le bon joueur, on surligne en rouge
                        if (_plateau.JoueurCourant != ((int)_plateau.trouverPiece(casePieceLeveeCourante) % 2))
                        {
                            changeStyleTagVisualizer(CaseDefinition.getVisu(casePieceLeveeCourante), 1, 100, System.Windows.Media.Brushes.Red);
                        }
                        //sinon on fait des trucs
                        else
                        {
                            casePosee = CaseXY.casePiecePosee(casesUI, casesPlateau);
                            surlignageCasesPossibles(_plateau.coupsPossibles(casePieceLeveeCourante));
                            
                            List<Case> listeCasesLevees = CaseXY.casesPiecesLevees(casesUI, casesPlateau);
                            foreach (Case c in listeCasesLevees)
                            {
                                Console.WriteLine("Case Levée : " + c);
                            }

                            if (listeCasesLevees.Count() == 2)
                            {
                                deplacementCapture = trouverDeplacement(_plateau.coupsPossibles(listeCasesLevees.ElementAt(0)), listeCasesLevees.ElementAt(1));
                                //Console.WriteLine(deplacementCapture.ToString());
                                
                            }
                            if (casesAutorisees(casePieceLeveeCourante).Contains(casePosee))
                            {
                                
                                desurlignageCases();
                            }

                            else if (casePosee != Case.NULL_CASE)
                            {
                                changeStyleTagVisualizer(CaseDefinition.getVisu(casePosee), 1, 100, System.Windows.Media.Brushes.Red);
                            }
                        }
                        
                    }
                    else
                    {
                        desurlignageCases();
                    }
                    derniereCaseLevee = casePieceLeveeCourante;
                    
                    /*
                    //Affiche les déplacements possibles pour une case
                    Case caseVide = CaseXY.casePieceLevee(casesUI, casesPlateau);
                    Case casePosee = CaseXY.casePiecePosee(casesUI, casesPlateau);
                    Console.WriteLine("case : " + caseVide);
                    //Si il y a une différence
                    if (caseVide != Case.NULL_CASE)
                    {

                        //On vérifie que la dernière pièce levée est bien la même afin d'éviter les faux "contacts"
                        if ((int)caseVide == (int)derniereCaseLevee)
                        {
                            // Si on a pas encore posée la pièce sur une autre case on affiche les cases dispo
                            if (casePosee == Case.NULL_CASE)
                            {
                                if (!isPossibleCapture)
                                {
                                    CasePieceLevee = caseVide;
                                    //si c'est au bon joueur de jouer
                                    if (_plateau.JoueurCourant == ((int)_plateau.trouverPiece(CasePieceLevee) % 2))
                                    {
                                        changeStyleTagVisualizer(CaseDefinition.getVisu(CasePieceLevee), 1, 100, System.Windows.Media.Brushes.Green);
                                        surlignageCasesPossibles(_plateau.coupsPossibles(CasePieceLevee));
                                    }
                                    else
                                    {
                                        changeStyleTagVisualizer(CaseDefinition.getVisu(CasePieceLevee), 1, 100, System.Windows.Media.Brushes.Red);
                                    }

                                }

                            }
                            else if (!casesAutorisees(caseVide).Contains(casePosee))
                            {
                                //Afficher la case en rouge (on a pas le droit d'y aller)
                                if (casePosee != Case.NULL_CASE)
                                    changeStyleTagVisualizer(CaseDefinition.getVisu(casePosee), 1, 100, System.Windows.Media.Brushes.Red);

                            }

                            // Si on a reposé la pièce alors on desurligne les cases
                            else
                            {
                                CasePiecePosee = casePosee;
                                desurlignageCases();
                            }
                        }

                    }
                    //Sinon on desurligne toutes les cases
                    else
                    {
                        isPossibleCapture = false;
                        desurlignageCases();
                    }
                    //Actualisation de la Piece levée
                    derniereCaseLevee = caseVide;*/
                }
                //Actualisation du temps pour les 100ms
               // _plateau.AfficherPlateau();
                oldTimeStamp = now;


            }
            // Si la table n'a pas réussi à nous envoyer une image
            if (!imageDisponible)
                return;
        }

        private List<Case> casesAutorisees(Case caseDepart)
        {
            List<Deplacement> deplacementAutorisees = _plateau.coupsPossibles(caseDepart);
            List<Case> casesAutorisees = new List<Case>();
            foreach (Deplacement dep in deplacementAutorisees)
            {
                casesAutorisees.Add(dep.caseDestination);
            }
            return casesAutorisees;
        }

        public bool jouerMouvement()
        {
            lock(this){
                if (_plateau.JoueurCourant == ((int)_plateau.trouverPiece(casePieceLeveeCourante) % 2))
                {
                
                    
                    Deplacement temp = trouverDeplacement(_plateau.coupsPossibles(casePieceLeveeCourante), casePosee); 
            
                    if (temp != null || deplacementCapture != null)
                    {
                        if (temp != null)
                        {
                            Console.WriteLine(temp.ToString());
                            if (temp.promotion)
                            {
                                if (_plateau.JoueurCourant == Joueur.COTE_BLANC)
                                {

                                    PartieGauche = new PromotionView(this, PartieGauche, temp);
                                }
                                else
                                {
                                    PartieDroite = new PromotionView(this, PartieDroite, temp);
                                }
                            }

                            _plateau.jouerMouvement(temp);
                        }
                        else if (deplacementCapture != null)
                        {
                            if (deplacementCapture.promotion)
                            {
                                if (_plateau.JoueurCourant == Joueur.COTE_BLANC)
                                {

                                    PartieGauche = new PromotionView(this, PartieGauche, deplacementCapture);
                                }
                                else
                                {
                                    PartieDroite = new PromotionView(this, PartieDroite, deplacementCapture);
                                }
                            }

                            _plateau.jouerMouvement(deplacementCapture);
                            deplacementCapture = null;
                        }
                        
                        if (_plateau.echecMat)
                        {
                            FenetrePrincipale.Fvm.Content = new FinPartieView(this);
                        }

                        isPossibleCapture = false;
                        return true;
                    }
                    return false;
                }
                else
                {
                    changeStyleTagVisualizer(CaseDefinition.getVisu(casePosee), 1, 100, System.Windows.Media.Brushes.Red);
                    return false;
                }
            }
            

        }

        private Deplacement trouverDeplacement(List<Deplacement> deplacements, Case caseDestination)
        {
            foreach (Deplacement dep in deplacements)
            {
                if (dep.caseDestination == caseDestination)
                {
                    return dep;
                }
            }
            return null;
        }

        /// <summary>
        /// Surlignagne des cases des coups possibles
        /// </summary>
        /// <param name="?"> Liste de cases atteignables par une pièce</param>
        public void surlignageCasesPossibles(List<Deplacement> deplacementAccessibles)
        {
            foreach (Deplacement monDeplacement in deplacementAccessibles)
            {
                CaseDefinition.getVisu(monDeplacement.caseDestination).BorderThickness = new Thickness(5);
                if (_plateau.caseEstLibre(monDeplacement.caseDestination))
                {
                    if (monDeplacement.typeDeplacement == TypeDeplacement.DEPLACEMENT_NORMAL)
                    {
                        CaseDefinition.getVisu(monDeplacement.caseDestination).BorderBrush = System.Windows.Media.Brushes.Yellow;
                    }
                    else if (monDeplacement.typeDeplacement == TypeDeplacement.DEPLACEMENT_ILLEGAL_ECHEC)
                    {
                        CaseDefinition.getVisu(monDeplacement.caseDestination).BorderBrush = System.Windows.Media.Brushes.Gray;
                    }
                }
                else if (!_plateau.caseEstLibre(monDeplacement.caseDestination) &&
                            _plateau.trouverPiece(monDeplacement.caseDestination) != Piece.ROI_BLANC &&
                               _plateau.trouverPiece(monDeplacement.caseDestination) != Piece.ROI_NOIR)
                {
                    isPossibleCapture = true;
                    CaseDefinition.getVisu(monDeplacement.caseDestination).BorderBrush = System.Windows.Media.Brushes.Orange;
                }
            }
        }

        public void changeStyleTagVisualizer(TagVisualizer t, double opacite, int thickness, SolidColorBrush sCD)
        {
            t.Opacity = opacite;
            t.BorderThickness = new Thickness(thickness);
            t.BorderBrush = sCD;
        }

        public void afficherImages(Case caseA)
        {

            Piece piece = _plateau.trouverPiece(caseA);

            if (piece == Piece.CASE_VIDE)
            {
                /*BitmapImage logo = new BitmapImage();
                logo.BeginInit();
                logo.UriSource = new Uri(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "ImageVide.png", UriKind.RelativeOrAbsolute);
                Console.WriteLine(logo.UriSource);
                logo.CacheOption = BitmapCacheOption.OnLoad;
                logo.
                logo.EndInit();*/
                CaseDefinition.listeCase.ElementAt((int)caseA).image.Source = null;

            }
            else
            {
                //Console.WriteLine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath));
                BitmapImage logo = new BitmapImage();
                logo.BeginInit();
                logo.UriSource = new Uri(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\"+ (int)piece + ".png", UriKind.RelativeOrAbsolute);
               // Console.WriteLine(logo.UriSource);
                logo.CacheOption = BitmapCacheOption.OnLoad;
                logo.EndInit();
                CaseDefinition.listeCase.ElementAt((int)caseA).image.Source = logo;
                //Console.WriteLine(CaseDefinition.listeCase.ElementAt((int)caseA).image.Source);
            }
            NotifyPropertyChanged("yolo");
            /*
            CaseDefinition.getVisu(caseA).Background = new ImageBrush(new BitmapImage(
                new Uri( Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)+"\\Resources\\1.png",UriKind.RelativeOrAbsolute)));*/
        }

        /// <summary>
        /// Remise du style de base (désurlignage) du plateau entier
        /// </summary>
        /// <param name="casesAccessibles"></param>
        public void desurlignageCases()
        {
            foreach (CaseDefinition cd in CaseDefinition.listeCase)
            {
                cd.visu.BorderThickness = new Thickness(0);
            }
        }
        #region methodesImages
        /// <summary>
        /// Fonction qui permet de stocker l'image dans normalizedImage
        /// </summary>
        /// <param name="e">L'évènement fourni par la table</param>
        void captureImage(Microsoft.Surface.Core.FrameReceivedEventArgs e)
        {
            if (normalizedImage == null)
            {
                imageDisponible = e.TryGetRawImage(
                  Microsoft.Surface.Core.ImageType.Normalized,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Left,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Top,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Width,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Height,
                     out normalizedImage,
                     out imageMetrics,
                     out paddingLeft,
                     out paddingRight);
            }
            else
            {
                imageDisponible = e.UpdateRawImage(
                  Microsoft.Surface.Core.ImageType.Normalized,
                   normalizedImage,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Left,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Top,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Width,
                  Microsoft.Surface.Core.InteractiveSurface.
                   PrimarySurfaceDevice.Height);
            }
        }

        /// <summary>
        /// Fonction permettant de traiter l'image
        /// </summary>
        /// <returns>renvoie un tableau de cercle</returns>
        CircleF[] traitementImage()
        {
            GCHandle h = GCHandle.Alloc(normalizedImage, GCHandleType.Pinned);
            IntPtr ptr = h.AddrOfPinnedObject();
            Bitmap imageBitmap = new Bitmap(imageMetrics.Width,
                                  imageMetrics.Height,
                                  imageMetrics.Stride,
                                  System.Drawing.Imaging.PixelFormat.Format8bppIndexed,
                                  ptr);
            //On transforme la bitmap en niveaux de gris
            Convert8bppBMPToGrayscale(imageBitmap);
            //Conversion de l'image en Emgu.Image
            Image<Bgr, Byte> img = new Image<Bgr, byte>(imageBitmap);
            //Conversion en gris afin de réduire le bruit
            UMat uimage = new UMat();
            CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);
            //use image pyr to remove noise
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(uimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, uimage);

            double cannyThreshold = 40.0;
            double circleAccumulatorThreshold = 10.0;
            CircleF[] circles = CvInvoke.HoughCircles(uimage, // L'image que l'on veut
                HoughType.Gradient,// constante qui bouge pas
                1.0,// Résolution de l'accu
                15.0, //Taille min entre chaque cercle
                cannyThreshold, // Seuil canny
                circleAccumulatorThreshold, // Plus c'est petit plus il y a de cercles
                10, 15); // Taille min et max des cercles
            return circles;
        }

        private void Convert8bppBMPToGrayscale(Bitmap bmp)
        {
            if (pal == null) // pal is defined at module level as --- ColorPalette pal;
            {
                pal = bmp.Palette;
                for (int i = 0; i < 256; i++)
                {
                    pal.Entries[i] = System.Drawing.Color.FromArgb(i, i, i);
                }
            }

            bmp.Palette = pal;
        }
        #endregion

        #endregion

        #region GETTERS ET SETTERS
        public Partie Partie
        {
            get { return _partie; }
            set
            {
                _partie = value;
                NotifyPropertyChanged("Partie");
            }
        }

        public UserControl PartieGauche
        {
            get { return _partieGauche; }
            set
            {
                _partieGauche = value;
                NotifyPropertyChanged("PartieGauche");
            }
        }

        public UserControl PartieDroite
        {
            get { return _partieDroite; }
            set
            {
                _partieDroite = value;
                NotifyPropertyChanged("PartieDroite");
            }
        }

        public UserControl InterfacePlateau
        {
            get { return _interfacePlateau; }
            set
            {
                _interfacePlateau = value;
                NotifyPropertyChanged("InterfacePlateau");
            }
        }

        public Plateau Plateau
        {
            get { return _plateau; }
            set
            {
                _plateau = value;
            }
        }
        # endregion

    }
}

