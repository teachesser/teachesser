﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Teachesser.ViewModel;

namespace Teachesser.View
{
    /// <summary>
    /// Logique d'interaction pour OptionPartieView.xaml
    /// </summary>
    /// <author> Raphaël Turquet </author>
    public partial class OptionPartieView : UserControl
    {
        private static OptionPartieView optv = new OptionPartieView();
        private OptionPartieViewModel optvm = new OptionPartieViewModel();

        #region CONSTRUCTEURS
        /// <summary>
        /// Constructeur
        /// </summary>
        public OptionPartieView()
        {
            this.DataContext = optv;
            InitializeComponent();
        }
        #endregion

        public static UserControl getInstance() { return optv; }

        /// <summary>
        /// Retour à l'accueil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void ChangeToAccueilView(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = View.AccueilView.getInstance();
        }

        /// <summary>
        /// Allez sur le plateau de jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void ChangeToInterfacePlateau(object sender, RoutedEventArgs e)
        {
            FenetrePrincipale.Fvm.Content = View.InterfacePlateau.getInstance();
        }
    }
}
