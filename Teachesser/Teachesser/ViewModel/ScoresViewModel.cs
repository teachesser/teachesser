﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Teachesser.Model;
using Teachesser.Model.BDDScores;

/// <summary>
/// View Model pour le tableau des scores
/// </summary>
/// <author> Raphaël Turquet </author>
namespace Teachesser.ViewModel
{
    class ScoresViewModel : ViewModelBase
    {
        private ObservableCollection <ProfilJoueur> _data;

        public ScoresViewModel()
        {
            Data = GestionnaireBDD.ReadXML();
        }

        public void rafraichir()
        {
            Data = GestionnaireBDD.ReadXML();
        }

        #region GETTERS ET SETTERS
        public ObservableCollection<ProfilJoueur> Data
        {
            get{return _data;}
            set
            {
                _data = value;
                NotifyPropertyChanged("Data");
            }
        }
        #endregion
    }
}
